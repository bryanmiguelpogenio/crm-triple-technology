﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.OrderTran;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface IOrderTransactionRepository : IRepository<ORDERTRAN>
    {
        List<ORDERTRAN> getActiveEntities(OrderTranViewModel model);
        List<ORDERTRAN> getForTransactionEntities(OrderTranViewModel model);
        List<ORDERTRAN> getEntities(OrderTranViewModel model);
        ORDERTRAN getActiveEntityByUserID(int id);
        ORDERTRAN GetNotActiveExistingTransaction(int id);
    }
}
