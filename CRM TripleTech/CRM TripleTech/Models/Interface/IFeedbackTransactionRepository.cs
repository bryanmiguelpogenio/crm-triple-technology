﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CRM_TripleTech.Models.Interface
{
    public interface IFeedbackTransactionRepository : IRepository<FEEDBACKTRAN>
    {
        List<FEEDBACKTRAN> SearchFilter(FeedbackViewModel model, Expression<Func<FEEDBACKTRAN, bool>> predicate);
        List<FEEDBACKTRAN> SearchFilter(FeedbackViewModel model);
    }
}
