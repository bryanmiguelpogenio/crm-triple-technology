﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Role;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface IRoleRepository : IRepository<ROLE>
    {
        List<ROLE> SearchFilter(RoleViewModel model);
    }
}
