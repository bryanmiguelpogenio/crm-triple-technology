﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Service;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface IServiceRepository : IRepository<SERVICE>
    {
        List<SERVICE> SearchFilter(ServiceViewModel model);
    }
}
