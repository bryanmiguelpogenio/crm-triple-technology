﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.PurchaseOrder;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CRM_TripleTech.Models.Interface
{
    public interface IPurchaseOrderRepository : IRepository<PURCHASEORDER>
    {
       List<PURCHASEORDER> getEntitites(PurchaseOrderViewModel model, Expression<Func<PURCHASEORDER, bool>> predicate);
        List<PURCHASEORDER> getEntitites(PurchaseOrderViewModel model);
    }
}
