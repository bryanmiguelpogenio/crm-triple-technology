﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.ServiceTran;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CRM_TripleTech.Models.Interface
{
    public interface IServiceTransactionRepository : IRepository<SERVICETRAN>
    {
        List<SERVICETRAN> SearchFilter(ServiceTranViewModel model);
        List<SERVICETRAN> SearchFilter(ServiceTranViewModel model, Expression<Func<SERVICETRAN, bool>> predicate);
    }
}
