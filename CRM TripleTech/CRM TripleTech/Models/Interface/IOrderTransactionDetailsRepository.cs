﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface IOrderTransactionDetailsRepository : IRepository<ORDERTRANDETAIL>
    {
        List<ORDERTRANDETAIL> getActiveEntitiesByUserID(int id);
    }
}
