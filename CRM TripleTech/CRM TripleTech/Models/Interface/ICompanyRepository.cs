﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Company;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface ICompanyRepository : IRepository<COMPANY>
    {
        List<COMPANY> SearchFilter(CompanyViewModel model);
    }
}
