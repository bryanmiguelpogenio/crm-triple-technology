﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Category;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface ICategoryRepository : IRepository<CATEGORY>
    {
        List<CATEGORY> SearchFilter(CategoryViewModel model);
    }
}
