﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;

namespace CRM_TripleTech.Models.Interface
{
    public interface IDBEntity
    {
        DbSet<TEntity> Set<TEntity>() where TEntity : class;
        DbEntityEntry Entry(object entity);
        int SaveChanges();
        void Dispose();
    }
}
