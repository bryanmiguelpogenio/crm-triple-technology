﻿using CRM_TripleTech.Models.Entities;

namespace CRM_TripleTech.Models.Interface
{
    public interface IUserServiceTransactionRepository : IRepository<USERSERVICESCHED>
    {
        USERSERVICESCHED getEntityByServiceTranID(int id);
    }
}
