﻿using System;

namespace CRM_TripleTech.Models.Interface
{
    public interface IUnitOfWork : IDisposable
    {
        IAddressRepository Address { get; }
        ICompanyRepository Company { get; }
        ICategoryRepository Category { get; }
        IFeedbackTransactionRepository FeedbackTran { get; }
        IInventoryRepository Inventory { get; }
        IOrderTransactionDetailsRepository OrderTranDetail { get; }
        IOrderTransactionRepository OrderTran { get; }
        IRoleRepository Role { get; }
        IServiceRepository Service { get; }
        IServiceTransactionRepository ServiceTran { get; }
        IUserRepository User { get; }
        IUserServiceTransactionRepository UserServiceTran { get; }
        IPurchaseOrderRepository PurchaseOrder { get; }
        int SaveChanges();
    }
}
