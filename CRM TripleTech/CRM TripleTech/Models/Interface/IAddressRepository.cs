﻿using CRM_TripleTech.Models.Entities;

namespace CRM_TripleTech.Models.Interface
{
    public interface IAddressRepository : IRepository<ADDRESS>
    {
        ADDRESS getLatestEntityByUserID(int id);
    }
}
