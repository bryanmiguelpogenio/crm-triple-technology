﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.User;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.Interface
{
    public interface IUserRepository : IRepository<USER>
    {
        USER getEntityByUserName(string value);
        List<USER> SearchFilter(UserViewModel model);
    }
}
