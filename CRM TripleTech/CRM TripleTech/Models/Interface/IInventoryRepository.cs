﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace CRM_TripleTech.Models.Interface
{
    public interface IInventoryRepository : IRepository<INVENTORY>
    {
        bool IsStockHasMoreQuantity(int id, int quantity);
        List<INVENTORY> SearchFilter(InventoryViewModel model);
        List<INVENTORY> SearchFilter(InventoryViewModel model, Expression<Func<INVENTORY, bool>> predicate);
    }
}
