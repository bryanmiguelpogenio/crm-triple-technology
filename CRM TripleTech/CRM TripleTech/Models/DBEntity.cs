namespace CRM_TripleTech.Models
{
    using CRM_TripleTech.Models.Entities;
    using CRM_TripleTech.Models.Interface;
    using System.Data.Entity;

    public partial class DBEntity : DbContext, IDBEntity
    {
        public DBEntity()
            : base("name=DBEntity")
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<DBEntity>());
        }

        public virtual DbSet<ADDRESS> Address { get; set; }
        public virtual DbSet<CATEGORY> Category { get; set; }
        public virtual DbSet<COMPANY> Company { get; set; }
        public virtual DbSet<FEEDBACKTRAN> FeedBackTran { get; set; }
        public virtual DbSet<INVENTORY> Inventory { get; set; }
        public virtual DbSet<ORDERTRAN> OrderTran { get; set; }
        public virtual DbSet<ORDERTRANDETAIL> OrderTranDetail { get; set; }
        public virtual DbSet<ROLE> Role { get; set; }
        public virtual DbSet<SERVICE> Service { get; set; }
        public virtual DbSet<SERVICETRAN> ServiceTran { get; set; }
        public virtual DbSet<USER> User { get; set; }
        public virtual DbSet<USERSERVICESCHED> UserServiceSched { get; set; }
        public virtual DbSet<PURCHASEORDER> PurchaseOrder { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            ////--> User to Role
            //modelBuilder.Entity<USER>().HasOptional(s => s.ROLE).WithMany(s => s.USERS).Map(s => s.ROLE_ID);

            ////--> User to Company
            //modelBuilder.Entity<USER>().HasOptional(s => s.COMPANY).WithMany(s => s.USERS).HasForeignKey(s => s.COMPANY_ID);

            ////--> Client to User
            //modelBuilder.Entity<FEEDBACKTRAN>().HasOptional(s => s.CLIENT).WithMany(s => s.ASCLIENTS).HasForeignKey(s => s.CLIENT_ID);

            ////--> Admin to User
            //modelBuilder.Entity<FEEDBACKTRAN>().HasOptional(s => s.ADMIN).WithMany(s => s.ASADMINS).HasForeignKey(s => s.ADMIN_ID);

            ////--> Inventory to Category
            //modelBuilder.Entity<INVENTORY>().HasOptional(s => s.CATEGORY).WithMany(s => s.INVENTORIES).HasForeignKey(s => s.CATEGORY_ID);

            ////--> OrderTran to User
            //modelBuilder.Entity<ORDERTRAN>().HasOptional(s => s.USER).WithMany(s => s.ASORDERUSERS).HasForeignKey(s => s.USER_ID);

            ////--> OrderTran to OrderTranDetail
            //modelBuilder.Entity<ORDERTRAN>().HasOptional(s => s.ORDERTRANDETAIL).WithMany(s => s.ORDERTRANS).HasForeignKey(s => s.ORDERTRANDETAIL_ID);

            ////--> OrderTranDetail to Inventory
            //modelBuilder.Entity<ORDERTRANDETAIL>().HasOptional(s => s.INVENTORY).WithMany(s => s.ORDERTRANDETAILS).HasForeignKey(s => s.INVENTORY_ID);

            ////--> ServiceTran to User
            //modelBuilder.Entity<SERVICETRAN>().HasOptional(s => s.USER).WithMany(s => s.ASSERVICEUSERS).HasForeignKey(s => s.USER_ID);

            ////--> ServiceTran to Service
            //modelBuilder.Entity<SERVICETRAN>().HasOptional(s => s.SERVICE).WithMany(s => s.SERVICETRANS).HasForeignKey(s => s.SERVICE_ID);

            ////--> UserServiceSched to User
            //modelBuilder.Entity<USERSERVICESCHED>().HasOptional(s => s.USER).WithMany(s => s.ASSERVICESCHEDUSERS).HasForeignKey(s => s.USER_ID);

            ////--> UserServiceSched to Service
            //modelBuilder.Entity<USERSERVICESCHED>().HasOptional(s => s.SERVICE).WithMany(s => s.USERSERVICESCHEDS).HasForeignKey(s => s.SERVICE_ID);

            ////--> Address to User
            //modelBuilder.Entity<ADDRESS>().HasOptional(s => s.USER).WithMany(s => s.ADDRESSES).HasForeignKey(s => s.USER_ID);
        }
    }
}
