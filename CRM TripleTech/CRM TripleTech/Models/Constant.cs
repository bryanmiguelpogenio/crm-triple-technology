﻿namespace CRM_TripleTech.Models
{
    public class Constant
    {
        public const int NO_USER_ID = 0;
        public const int NO_CATEGORY_ID = 0;
        public const int NO_ORDERTRAN_ID = 0;
        public const int NO_ADDRESS_ID = 0;

        public const int ROLE_ADMIN_ID = 1;
        public const int ROLE_CUSTOMER_ID = 2;
        public const int ROLE_TECHNICIAN_ID = 3;


        public const string ROLE_CREW = "Crew";

        public const string CATEGORY_MOTORS = "MOTOR";
        public const string CATEGORY_PUMPS = "PUMP";
        public const string CATEGORY_CONTROLLERS = "CONTROLLER";

        public const string MODAL_ADD = "ModalAdd";
        public const string MODAL_EDIT = "ModalEdit";
        public const string MODAL_CART = "ModalCart";
        public const string MODAL_SCHED = "ModalSchedule";
        public const string MODAL_REPLENISH = "ModalReplenish";
        public const string CONFIRMATION_MESSAGE_DELETE = "Are you sure you want to delete this record?";
        public const string ALERT_MESSAGE_CANCELLED = "The item has been removed from your orders.";


        public const string NOT_AVAILABLE = "N/A";

        public const string CONFIRMATION_LOGOUT = "You are about to logout, continue?";

        public const string ORDERTRAN_STATUS_ACTIVE = "ACTIVE";
        public const string ORDERTRAN_STATUS_PROCESSING = "PROCESSING";
        public const string ORDERTRAN_STATUS_SHIPPING = "SHIPPING";
        public const string ORDERTRAN_STATUS_COMPLETED = "COMPLETED";
        public const string ORDERTRAN_STATUS_CANCELLED = "CANCELLED";

        public const string SERVICE_STATUS_ACTIVE = "ACTIVE";
        public const string SERVICE_STATUS_PROCESSING = "PROCESSING";
        public const string SERVICE_STATUS_ONGOING = "ON-GOING";
        public const string SERVICE_STATUS_COMPLETED = "COMPLETED";

        public const string PURCHASE_ORDER_STATUS_PROCESSING = "PROCESSING";
        public const string PURCHASE_ORDER_STATUS_RECEIVED = "RECEIVED";

        public const string ORDERTRAN_TRANSACTION_TYPE_SHIP = "SHIP";
        public const string ORDERTRAN_TRANSACTION_TYPE_CANCEL = "CANCEL";
        public const string ORDERTRAN_TRANSACTION_TYPE_COMPLETE = "COMPLETE";

        public const string PRIORITY_LEVEL_HIGH = "HIGH";
        public const string PRIORITY_LEVEL_MED = "MEDIUM";
        public const string PRIORITY_LEVEL_LOW = "LOW";

        public const string ALERT_ITEM_ADDED = "Item has been added to cart!";

        public const string ACTION_TYPE_NEW = "NEW";
        public const string ACTION_TYPE_EDIT= "EDIT";
        public const string ACTION_TYPE_DELETE = "DELETE";
        
        public const string DEFAULT_PASSWORD = "Login123";

        public const string DELIVERY_TYPE = "Cash On Delivery";

        public const string COMPANY_TYPE_VENDOR = "Vendor";
        public const string COMPANY_TYPE_NON_VENDOR = "Non-Vendor";

        public const string DEFAULT_CODE = "CODE000";
        public const string DEFAULT_DELIVERY_ADDRESS = "Tripletech Corporate Building, 466 Sta.Scholastica St.San Antonio Valley 1, Brgy.San Antonio, Paranaque City";
    }
}