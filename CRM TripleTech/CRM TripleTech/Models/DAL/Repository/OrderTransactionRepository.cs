﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.OrderTran;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class OrderTransactionRepository : Repository<ORDERTRAN>, IOrderTransactionRepository
    {
        public readonly IDBEntity _dbEntity;

        public OrderTransactionRepository(IDBEntity dbEntity)
            : base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<ORDERTRAN> getActiveEntities(OrderTranViewModel model)
        {
            var query = _dbEntity.Set<ORDERTRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Status))
            {
                query = query.Where(m => m.STATUS == model.Status);
            }

            query = query.Where(m => m.STATUS != Constant.ORDERTRAN_STATUS_CANCELLED
            && m.STATUS != Constant.ORDERTRAN_STATUS_COMPLETED);

            return query.ToList();
        }

        public List<ORDERTRAN> getForTransactionEntities(OrderTranViewModel model)
        {
            var query = _dbEntity.Set<ORDERTRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Status))
            {
                query = query.Where(m => m.STATUS == model.Status);
            }

            query = query.Where(m => m.STATUS != Constant.ORDERTRAN_STATUS_CANCELLED
            && m.STATUS != Constant.ORDERTRAN_STATUS_COMPLETED && m.STATUS != Constant.ORDERTRAN_STATUS_ACTIVE);

            return query.ToList();
        }

        public List<ORDERTRAN> getEntities(OrderTranViewModel model)
        {
            var query = _dbEntity.Set<ORDERTRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Status))
            {
                query = query.Where(m => m.STATUS == model.Status);
            }

            return query.ToList();
        }

        public ORDERTRAN getActiveEntityByUserID(int id)
        {
            return _dbEntity.Set<ORDERTRAN>().Where(m => m.USER_ID == id & m.STATUS == Constant.ORDERTRAN_STATUS_ACTIVE).FirstOrDefault();
        }

        public ORDERTRAN GetNotActiveExistingTransaction(int id)
        {
            return _dbEntity.Set<ORDERTRAN>().Where(m => m.USER_ID == id & m.STATUS != Constant.ORDERTRAN_STATUS_ACTIVE).FirstOrDefault();
        }
    }
}