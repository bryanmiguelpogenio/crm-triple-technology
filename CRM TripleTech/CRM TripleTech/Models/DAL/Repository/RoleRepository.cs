﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Role;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class RoleRepository : Repository<ROLE>, IRoleRepository
    {
        public readonly IDBEntity _dbEntity;

        public RoleRepository(IDBEntity dbEntity) :
            base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<ROLE> SearchFilter(RoleViewModel model)
        {
            var query = _dbEntity.Set<ROLE>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                query = query.Where(m => m.NAME == model.Name);
            }

            return Find(query).ToList();
        }
    }
}