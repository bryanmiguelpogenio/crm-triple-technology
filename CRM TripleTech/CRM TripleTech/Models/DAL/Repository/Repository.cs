﻿using CRM_TripleTech.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {

        private readonly IDBEntity _dbEntity;

        public Repository(IDBEntity dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public TEntity Get(int id)
        {
            return _dbEntity.Set<TEntity>().Find(id);
        }

        public IEnumerable<TEntity> GetAll()
        {
            return _dbEntity.Set<TEntity>().ToList();
        }

        public IEnumerable<TEntity> Find(Expression<Func<TEntity, bool>> predicate)
        {
            return _dbEntity.Set<TEntity>().Where(predicate);
        }

        public IEnumerable<TEntity> Find(IQueryable<TEntity> source)
        {
            return source.AsEnumerable();
        }

        public void Add(TEntity entity)
        {
            _dbEntity.Set<TEntity>().Add(entity);
        }

        public void AddRange(IEnumerable<TEntity> entities)
        {
            _dbEntity.Set<TEntity>().AddRange(entities);
        }

        public void Update(TEntity entity)
        {
            _dbEntity.Entry(entity).State = EntityState.Modified;
        }

        public void Remove(TEntity entity)
        {
            _dbEntity.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            _dbEntity.Set<TEntity>().RemoveRange(entities);
        }
    }
}