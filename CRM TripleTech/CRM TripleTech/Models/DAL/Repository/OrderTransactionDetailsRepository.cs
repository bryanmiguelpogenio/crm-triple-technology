﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class OrderTransactionDetailsRepository : Repository<ORDERTRANDETAIL>, IOrderTransactionDetailsRepository
    {
        public readonly IDBEntity _dbEntity;

        public OrderTransactionDetailsRepository(IDBEntity dbEntity)
            : base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<ORDERTRANDETAIL> getActiveEntitiesByUserID(int id)
        {
            var list = _dbEntity.Set<ORDERTRANDETAIL>().Where(m => m.STATUS == Constant.ORDERTRAN_STATUS_ACTIVE & m.ORDERTRAN.USER_ID == id).ToList();

            return list;
        }
    }
}