﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class UserServiceTransactionRepository : Repository<USERSERVICESCHED>, IUserServiceTransactionRepository
    {
        public readonly IDBEntity _dbEntity;

        public UserServiceTransactionRepository(IDBEntity dbEntity)
            : base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public USERSERVICESCHED getEntityByServiceTranID(int id)
        {
            return _dbEntity.Set<USERSERVICESCHED>().Where(m => m.SERVICE_ID == id).FirstOrDefault();
        }
    }
}