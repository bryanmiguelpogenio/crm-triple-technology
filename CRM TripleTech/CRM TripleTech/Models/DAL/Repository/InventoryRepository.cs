﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Inventory;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class InventoryRepository : Repository<INVENTORY>, IInventoryRepository
    {
        public readonly IDBEntity _dbEntity;

        public InventoryRepository(IDBEntity dbEntity)
            : base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public bool IsStockHasMoreQuantity(int id, int quantity)
        {
            var model = _dbEntity.Set<INVENTORY>().Where(m => m.ID == id).FirstOrDefault();

            if (model.QUANTITY >= quantity)
                return true;
            else
                return false;
        }

        public List<INVENTORY> SearchFilter(InventoryViewModel model)
        {
            var query = _dbEntity.Set<INVENTORY>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                query = query.Where(m => m.NAME == model.Name);
            }

            if (model.CategoryID != 0)
            {
                query = query.Where(m => m.CATEGORY_ID == model.CategoryID);
            }

            return query.ToList();
        }

        public List<INVENTORY> SearchFilter(InventoryViewModel model, Expression<Func<INVENTORY, bool>> predicate)
        {
            var query = _dbEntity.Set<INVENTORY>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                query = query.Where(m => m.NAME == model.Name);
            }

            if (model.CategoryID != 0)
            {
                query = query.Where(m => m.CATEGORY_ID == model.CategoryID);
            }

            query = query.Where(predicate);

            return query.ToList();
        }
    }
}