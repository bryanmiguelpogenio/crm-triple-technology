﻿using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using System.Linq;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class AddressRepository : Repository<ADDRESS>, IAddressRepository
    {
        public readonly IDBEntity _dbEntity;

        public AddressRepository(IDBEntity dbEntity) :
            base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public ADDRESS getLatestEntityByUserID(int id)
        {
            return _dbEntity.Set<ADDRESS>().Where(m => m.USER_ID == id).OrderByDescending(m => m.ID).FirstOrDefault();
        }
    }
}