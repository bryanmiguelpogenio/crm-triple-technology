﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.PurchaseOrder;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class PurchaseOrderRepository : Repository<PURCHASEORDER>, IPurchaseOrderRepository
    {
        public readonly IDBEntity _dbEntity;

        public PurchaseOrderRepository(IDBEntity dbEntity) :
            base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<PURCHASEORDER> getEntitites(PurchaseOrderViewModel model, Expression<Func<PURCHASEORDER, bool>> predicate)
        {
            var query = _dbEntity.Set<PURCHASEORDER>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.CompanyID))
            {
                int.TryParse(model.CompanyID, out value);
                query = query.Where(m => m.COMPANY_ID == value);
            }

            query = query.Where(predicate);

            return query.ToList();
        }

        public List<PURCHASEORDER> getEntitites(PurchaseOrderViewModel model)
        {
            var query = _dbEntity.Set<PURCHASEORDER>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.CompanyID))
            {
                int.TryParse(model.CompanyID, out value);
                query = query.Where(m => m.COMPANY_ID == value);
            }

            return query.ToList();
        }
    }
}