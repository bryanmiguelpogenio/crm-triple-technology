﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class FeedbackTransactionRepository : Repository<FEEDBACKTRAN>, IFeedbackTransactionRepository
    {
        public readonly IDBEntity _dbEntity;

        public FeedbackTransactionRepository(IDBEntity dbEntity) :
            base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<FEEDBACKTRAN> SearchFilter(FeedbackViewModel model, Expression<Func<FEEDBACKTRAN, bool>> predicate)
        {
            var query = _dbEntity.Set<FEEDBACKTRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.ClientID))
            {
                int.TryParse(model.ClientID, out value);
                query = query.Where(m => m.CLIENT_ID == value);
            }

            query = query.Where(predicate);

            return query.ToList();
        }

        public List<FEEDBACKTRAN> SearchFilter(FeedbackViewModel model)
        {
            var query = _dbEntity.Set<FEEDBACKTRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.ClientID))
            {
                int.TryParse(model.ClientID, out value);
                query = query.Where(m => m.CLIENT_ID == value);
            }

            return query.ToList();
        }
    }
}