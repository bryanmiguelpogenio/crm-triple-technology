﻿using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class UserRepository : Repository<USER>, IUserRepository
    {
        public readonly IDBEntity _dbEntity;

        public UserRepository(IDBEntity dbEntity)
            : base(dbEntity)

        {
            this._dbEntity = dbEntity;
        }

        public USER getEntityByUserName(string value)
        {
            return _dbEntity.Set<USER>().Where(m => m.USERNAME == value).FirstOrDefault();
        }

        public List<USER> SearchFilter(UserViewModel model)
        {
            var query = _dbEntity.Set<USER>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (model.CompanyID != 0)
            {
                query = query.Where(m => m.COMPANY_ID == model.CompanyID);
            }

            if (!string.IsNullOrEmpty(model.CustomerName))
            {
                int.TryParse(model.CustomerName, out value);
                query = query.Where(m => m.ID == value);
            }

            if (model.RoleID != 0)
            {
                query = query.Where(m => m.ROLE_ID == model.RoleID);
            }

            return Find(query).ToList();
        }
    }
}