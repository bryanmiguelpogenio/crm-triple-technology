﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.ServiceTran;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class ServiceTransactionRepository : Repository<SERVICETRAN>, IServiceTransactionRepository
    {
        public readonly IDBEntity _dbEntity;

        public ServiceTransactionRepository(IDBEntity dbEntity)
            : base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<SERVICETRAN> SearchFilter(ServiceTranViewModel model)
        {
            var query = _dbEntity.Set<SERVICETRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.UserID))
            {
                int.TryParse(model.UserID, out value);
                query = query.Where(m => m.USER_ID == value);
            }

            if (!string.IsNullOrEmpty(model.ServiceID))
            {
                int.TryParse(model.UserID, out value);
                query = query.Where(m => m.SERVICE_ID == value);
            }

            return Find(query).ToList();
        }

        public List<SERVICETRAN> SearchFilter(ServiceTranViewModel model, Expression<Func<SERVICETRAN, bool>> predicate)
        {
            var query = _dbEntity.Set<SERVICETRAN>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.UserID))
            {
                int.TryParse(model.UserID, out value);
                query = query.Where(m => m.USER_ID == value);
            }

            if (!string.IsNullOrEmpty(model.ServiceID))
            {
                int.TryParse(model.UserID, out value);
                query = query.Where(m => m.SERVICE_ID == value);
            }

            query = query.Where(predicate);

            return query.ToList();
        }
    }
}