﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Category;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL.Repository
{
    public class CategoryRepository : Repository<CATEGORY>, ICategoryRepository
    {
        public readonly IDBEntity _dbEntity;

        public CategoryRepository(IDBEntity dbEntity)
            : base(dbEntity)
        {
            this._dbEntity = dbEntity;
        }

        public List<CATEGORY> SearchFilter(CategoryViewModel model)
        {
            var query = _dbEntity.Set<CATEGORY>().AsQueryable();
            int value = 0;

            if (!string.IsNullOrEmpty(model.ID))
            {
                int.TryParse(model.ID, out value);
                query = query.Where(m => m.ID == value);
            }

            if (!string.IsNullOrEmpty(model.Name))
            {
                query = query.Where(m => m.NAME == model.Name);
            }

            return Find(query).ToList();
        }
    }
}