﻿using CRM_TripleTech.Models;
using System.Linq;
using System.IO;
using System.Web;
using Autofac;
using CRM_TripleTech.Models.Interface;
using System;

namespace CRM_TripleTech.Models.DAL
{
    public class StaticMethods
    {
        //--> Convert object to Integer
        public static int? TryParseObjToInt(object value)
        {
            int retval = 0;
            int.TryParse(value != null ? value.ToString() : string.Empty, out retval);

            return retval;
        }

        public static int? TryParseStringToInt(string value)
        {
            int retval = 0;
            int.TryParse(value != null ? value.ToString() : string.Empty, out retval);

            return retval;
        }

        //--> Check if user has access to admin page.
        public static bool HasAdminAccess(int? id)
        {
            if (id.HasValue && Constant.ROLE_ADMIN_ID == id.Value) return true;
            else return false;
        }

        public static bool HasActiveOrderTransaction(int? id)
        {
            bool retval = false;

            var container = ContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var unitOfWork = scope.Resolve<IUnitOfWork>();

                if (unitOfWork.OrderTran.getActiveEntityByUserID(id.Value) != null)
                    retval = true;
            }

            return retval;
        }

        public static bool CustomerHasTransaction(int? id)
        {
            bool retval = false;

            var container = ContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var unitOfWork = scope.Resolve<IUnitOfWork>();

                if (unitOfWork.OrderTran.GetNotActiveExistingTransaction(id.Value) != null)
                    retval = true;
            }

            return retval;
        }

        public static int GetActiveItems(int? id)
        {
            int retval = 0;

            var container = ContainerConfig.Configure();
            using (var scope = container.BeginLifetimeScope())
            {
                var unitOfWork = scope.Resolve<IUnitOfWork>();

                var model = unitOfWork.OrderTran.getActiveEntityByUserID(id.Value);
                if (model != null)
                    retval = model.ORDERTRANDETAIL.Where(m => m.STATUS == Constant.ORDERTRAN_STATUS_ACTIVE).ToList().Count;
            }

            return retval;
        }

        public static bool ProductFileExists(string path)
        {
            return File.Exists(HttpContext.Current.Server.MapPath(path));
        }

        public static string CreatePurchaseNo(string id)
        {
            string retval = "P";

            if (id.Length == 1) retval += id + "000";
            else if (id.Length == 2) retval += id + "00";
            else if (id.Length == 3) retval += id + "0";
            else if (id.Length == 4) retval += id;

            retval += DateTime.UtcNow.Year.ToString().Substring(DateTime.UtcNow.Year.ToString().Length - 2);

            return retval;
        }

        public static string CreateOrderNo(string id)
        {
            string retval = "O";

            if (id.Length == 1) retval += id + "000";
            else if (id.Length == 2) retval += id + "00";
            else if (id.Length == 3) retval += id + "0";
            else if (id.Length == 4) retval += id;

            retval += DateTime.UtcNow.Year.ToString().Substring(DateTime.UtcNow.Year.ToString().Length - 2);

            return retval;
        }
    }
}