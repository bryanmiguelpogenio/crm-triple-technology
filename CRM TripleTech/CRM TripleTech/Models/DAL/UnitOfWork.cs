﻿using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL.Repository;
using CRM_TripleTech.Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models.DAL
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly IDBEntity _dbEntity;

        #region COMMENT
        //public UnitOfWork(
        //    IDBEntity dBEntity, 
        //    IAddressRepository address, 
        //    ICompanyRepository company,
        //    ICategoryRepository category, 
        //    IFeedbackTransactionRepository feedbackTransaction, 
        //    IInventoryRepository inventory, 
        //    IOrderTransactionDetailsRepository orderTransactionDetails, 
        //    IOrderTransactionRepository orderTransaction, 
        //    IRoleRepository role,
        //    IServiceRepository service, 
        //    IServiceTransactionRepository serviceTransaction, 
        //    IUserRepository user, 
        //    IUserServiceTransactionRepository userServiceTransaction)
        //{
        //    this._dbEntity = dBEntity;

        //    //Address = new AddressRepository(_dbEntity);
        //    //Company = new CompanyRepository(_dbEntity);
        //    //Category = new CategoryRepository(_dbEntity);
        //    //FeedbackTran = new FeedbackTransactionRepository(_dbEntity);
        //    //Inventory = new InventoryRepository(_dbEntity);
        //    //OrderTranDetail = new OrderTransactionDetailsRepository(_dbEntity);
        //    //OrderTran = new OrderTransactionRepository(_dbEntity);
        //    //Role = new RoleRepository(_dbEntity);
        //    //Service = new ServiceRepository(_dbEntity);
        //    //ServiceTran = new ServiceTransactionRepository(_dbEntity);
        //    //User = new UserRepository(_dbEntity);
        //    //UserServiceTran = new UserServiceTransactionRepository(_dbEntity);

        //    Address = address;
        //    Company = company;
        //    Category = category;
        //    FeedbackTran = feedbackTransaction;
        //    Inventory = inventory;
        //    OrderTranDetail = orderTransactionDetails;
        //    OrderTran = orderTransaction;
        //    Role = role;
        //    Service = service;
        //    ServiceTran = serviceTransaction;
        //    User = user;
        //    UserServiceTran = userServiceTransaction;
        //}
        #endregion

        public UnitOfWork(IDBEntity dBEntity)
        {
            this._dbEntity = dBEntity;

            Address = new AddressRepository(_dbEntity);
            Company = new CompanyRepository(_dbEntity);
            Category = new CategoryRepository(_dbEntity);
            FeedbackTran = new FeedbackTransactionRepository(_dbEntity);
            Inventory = new InventoryRepository(_dbEntity);
            OrderTranDetail = new OrderTransactionDetailsRepository(_dbEntity);
            OrderTran = new OrderTransactionRepository(_dbEntity);
            Role = new RoleRepository(_dbEntity);
            Service = new ServiceRepository(_dbEntity);
            ServiceTran = new ServiceTransactionRepository(_dbEntity);
            User = new UserRepository(_dbEntity);
            UserServiceTran = new UserServiceTransactionRepository(_dbEntity);
            PurchaseOrder = new PurchaseOrderRepository(_dbEntity);
        }

        public IAddressRepository Address { get; private set; }
        public ICompanyRepository Company { get; private set; }
        public ICategoryRepository Category { get; private set; }
        public IFeedbackTransactionRepository FeedbackTran { get; private set; }
        public IInventoryRepository Inventory { get; private set; }
        public IOrderTransactionDetailsRepository OrderTranDetail { get; private set; }
        public IOrderTransactionRepository OrderTran { get; private set; }
        public IRoleRepository Role { get; private set; }
        public IServiceRepository Service { get; private set; }
        public IServiceTransactionRepository ServiceTran { get; private set; }
        public IUserRepository User { get; private set; }
        public IUserServiceTransactionRepository UserServiceTran { get; private set; }
        public IPurchaseOrderRepository PurchaseOrder { get; private set; }

        public int SaveChanges()
        {
            return _dbEntity.SaveChanges();
        }

        public void Dispose()
        {
            _dbEntity.Dispose();
        }
    }
}