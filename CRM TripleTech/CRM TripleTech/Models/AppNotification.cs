﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CRM_TripleTech.Models
{
    public class AppNotification
    {
        public string Header { get; set; }
        public string Body { get; set; }
        public string Class { get; set; }
    }
}