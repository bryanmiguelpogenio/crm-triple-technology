﻿using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Admin
{
    public class AdminViewModel
    {
        public List<LegendInventory> lst_LCategory { get; set; }
    }

    public class LegendInventory
    {
        public int ID { get; set; }
        public string Name { get; set; }
    }
}