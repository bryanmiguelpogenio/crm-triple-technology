﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.PurchaseOrder
{
    public class PurchaseOrderViewModel
    {
        //public string PurchaseOrderID { get; set; }
        //public string VendorCode { get; set; }
        //public string CompanyName { get; set; }
        //public string Quantity { get; set; }
        //public string DateOrdered { get; set; }
        //public string Amount { get; set; }

        public string ID { get; set; }
        public string CompanyID { get; set; }
        public List<SelectListItem> lst_Company { get; set; }  

        public List<PURCHASEORDER> lst_Main { get; set; }
    }
}