﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.OrderTran
{
    public class OrderTranViewModel
    {
        public string ID { get; set; }

        public List<OrderTranModel> lst_Main { get; set; }

        public string Status { get; set; }
        public List<SelectListItem> drp_Status { get; set; }
    }

    public class OrderTranModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Company { get; set; }
        public string PriorityLevel { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public double TotalPrice { get; set; }

        public DateTime? DateDelivered { get; set; }
        public DateTime? DateUpdated { get; set; }

        public string LastUpdatedBy { get; set; }

        public List<OrderTranDetailModel> lst_Main { get; set; }
    }

    public class OrderTranDetailModel
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
        public string Status { get; set; }
    }
}