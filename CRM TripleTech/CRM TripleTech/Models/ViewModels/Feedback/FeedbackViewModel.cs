﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Feedback
{
    public class FeedbackViewModel
    {
        public string ID { get; set; }
        public string Subject { get; set; }

        public string ClientID { get; set; }
        public List<SelectListItem> drp_Client { get; set; }

        public List<FeedbackModel> lst_Main { get; set; }
    }

    public class FeedbackModel
    {
        public string Id { get; set; }
        public string Subject { get; set; }
        public string CustomerName { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }
        public bool HasReplied { get; set; }
    }
}