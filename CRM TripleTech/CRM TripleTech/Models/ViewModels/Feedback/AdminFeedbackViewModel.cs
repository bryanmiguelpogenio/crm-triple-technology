﻿using System.ComponentModel.DataAnnotations;

namespace CRM_TripleTech.Models.ViewModels.Feedback
{
    public class AdminFeedbackViewModel
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public string ClientMessage { get; set; }
        public string CustomerName { get; set; }
        public string CompanyName { get; set; }
        public string Status { get; set; }

        [Required(ErrorMessage = "Message is required")]
        public string AdminMessage { get; set; }

    }
}