﻿namespace CRM_TripleTech.Models.ViewModels.Cart
{
    public class CartViewModel
    {
        //--> OrderTransactionId
        public int OrderTranId { get; set; }
        public int InventoryId { get; set; }

        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
    }
}