﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CRM_TripleTech.Models.ViewModels.Cart
{
    public class CartLocationViewModel
    {
        public int OrderTranId { get; set; }

        [Display(Name = "Unit/Room No., Floor")]
        public string UnitRmNoFloor { get; set; }
        [Display(Name = "Building Name")]
        public string BldgName { get; set; }
        [Display(Name = "Lot No., Block No., Phase No. House No.")]
        public string LotNoBlockNoPhaseNoHouseNo { get; set; }
        [Display(Name = "Street Name")]
        public string Street { get; set; }
        [Display(Name = "Subdivision")]
        public string Subdivision { get; set; }
        [Display(Name = "Barangay")]
        public string Barangay { get; set; }
        [Display(Name = "Municipality/City")]
        public string MunicipalityCity { get; set; }
        [Display(Name = "Province/State")]
        public string ProvinceState { get; set; }
        [Display(Name = "Zip Code")]
        public string ZipCode { get; set; }

        public DateTime? ScheduleDelivery { get; set; }
    }
}