﻿using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels
{
    //--> Main Data Interface
    public class ChartsDetails
    {
        public List<string> labels { get; set; }
        public List<double?> data { get; set; }
        public double? maxValue { get; set; }
    }

    public class ChartsDatasets
    {
        public List<string> labels { get; set; }
        public List<int> data { get; set; }
    }

    public class ChartsLabels
    {
        public List<string> labels { get; set; }
    }
}