﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Role
{
    public class RoleViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public List<ROLE> lst_Main { get; set; }
    }
}