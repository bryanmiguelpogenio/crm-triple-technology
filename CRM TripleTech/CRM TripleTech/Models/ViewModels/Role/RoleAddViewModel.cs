﻿using System.ComponentModel.DataAnnotations;

namespace CRM_TripleTech.Models.ViewModels.Role
{
    public class RoleAddViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}