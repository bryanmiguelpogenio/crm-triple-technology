﻿using System.ComponentModel.DataAnnotations;

namespace CRM_TripleTech.Models.ViewModels.Role
{
    public class RoleEditViewModel
    {
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}