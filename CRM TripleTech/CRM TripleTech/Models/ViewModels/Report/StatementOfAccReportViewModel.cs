﻿using CRM_TripleTech.Models.ViewModels.OrderTran;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class StatementOfAccReportViewModel
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public string Company { get; set; }
        public string DeliveryType { get; set; }
        public string DateOrdered { get; set; }
        public string DatePrinted { get; set; }
        public string TotalAmount { get; set; }

        public List<SOADetailModel> lst_Main { get; set; }

    }

    public class SOADetailModel
    {
        public int Id { get; set; }
        public string CategoryName { get; set; }
        public string Name { get; set; }
        public int Quantity { get; set; }
        public double Price { get; set; }
        public double Amount { get; set; }
    }


}