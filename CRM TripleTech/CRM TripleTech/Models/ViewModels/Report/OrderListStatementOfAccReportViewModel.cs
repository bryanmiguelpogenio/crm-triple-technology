﻿namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class OrderListStatementOfAccReportViewModel
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string Company { get; set; }
        public string PriorityLevel { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }
        public double TotalPrice { get; set; }
    }
}