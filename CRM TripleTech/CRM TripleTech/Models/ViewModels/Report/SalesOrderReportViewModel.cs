﻿namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class SalesOrderReportViewModel
    {
        public string SalesOrderID { get; set; }
        public string CustomerName { get; set; }
        public string CompanyName { get; set; }
        public string DateOrdered { get; set; }
        public string SalesPerson { get; set; }
        public string PayCode { get; set; }
        public string SalesTax { get; set; }
        public string Amount { get; set; }

    }
}