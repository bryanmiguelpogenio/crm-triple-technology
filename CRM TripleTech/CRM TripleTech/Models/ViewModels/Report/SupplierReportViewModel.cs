﻿namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class SupplierReportViewModel
    {
        public string SupplierCode { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
    }
}