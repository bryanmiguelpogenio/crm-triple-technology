﻿namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class PurchaseOrderReportViewModel
    {
        public string PurchaseOrderID { get; set; }
        public string VendorCode { get; set; }
        public string CompanyName { get; set; }
        public string DateOrdered { get; set; }
        public string BuyerName { get; set; }
        public string FreightCharge { get; set; }
        public string SalesTax { get; set; }
        public string Amount { get; set; }
    }
}