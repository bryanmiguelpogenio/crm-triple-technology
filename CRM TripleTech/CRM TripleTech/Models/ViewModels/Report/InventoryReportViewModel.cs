﻿namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class InventoryReportViewModel
    {
        public string InventoryID { get; set; }
        public string Name { get; set; }
        public string Category { get; set; }
        public string StockQuantity { get; set; }
        public string ShippedQuantity { get; set; }
        public string SoldQuantity { get; set; }
        public string TotalCost { get; set; }
    }
}