﻿namespace CRM_TripleTech.Models.ViewModels.Report
{
    public class ScheduleReportViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public string Client { get; set; }
        public string Company { get; set; }
        public string Assigned { get; set; }
        public string DateScheduled { get; set; }
        public string Status { get; set; }
    }
}