﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class TransactionViewModel
    {
        public List<ORDERTRAN> lst_order { get; set; }
        public List<SERVICETRAN> lst_service { get; set; }
        public List<FEEDBACKTRAN> lst_feedback { get; set; }
    }
}