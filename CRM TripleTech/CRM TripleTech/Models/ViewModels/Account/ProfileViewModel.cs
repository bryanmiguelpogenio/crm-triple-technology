﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class ProfileViewModel
    {
        public int USER_ID { get; set; }
        
        [Required(ErrorMessage = "Please select a role")]
        public int? ROLE_ID { get; set; }
        public List<SelectListItem> drp_Role { get; set; }

        public int? COMPANY_ID { get; set; }
        public List<SelectListItem> drp_Company { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        [Required(ErrorMessage = "Please enter your Firstname")]
        public string FIRSTNAME { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        [Required(ErrorMessage = "Please enter your Middlename")]
        public string MIDDLENAME { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        [Required(ErrorMessage = "Please enter your Lastname")]
        public string LASTNAME { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        public string CONTACTNO { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [Remote("Profile_IsEmailExist", "Account", AdditionalFields = "USER_ID", HttpMethod = "POST", ErrorMessage = "Email already exists. Please enter a different email.")]
        public string EMAILADDRESS { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        public string TELEPHONENO { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        [Required(ErrorMessage = "Username is required")]
        [Remote("Profile_IsUserNameExist", "Account", AdditionalFields = "USER_ID", HttpMethod = "POST", ErrorMessage = "Username already exists. Please enter a different username.")]
        public string USERNAME { get; set; }

        public int ADDRESS_ID { get; set; }

        public string ADDRESSNAME { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        public string BARANGAY { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        public string MUNICIPALITY { get; set; }

        [StringLength(5, ErrorMessage = "Maximum of 5 characters only")]
        public string POSTALCODE { get; set; }

        [StringLength(50, ErrorMessage = "Maximum of 50 characters only")]
        public string LOCALITY { get; set; }

        //public USER Main { get; set; }
        //public ADDRESS Main_Address { get; set; }
    }
}