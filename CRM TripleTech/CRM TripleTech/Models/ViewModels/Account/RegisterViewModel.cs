﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class RegisterViewModel
    {
        [Required(ErrorMessage = "Please enter your Firstname")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "Please enter your Middlename")]
        public string MiddleName { get; set; }

        [Required(ErrorMessage = "Please enter your Lastname")]
        public string LastName { get; set; }

        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [Remote("IsEmailNotYetExist", "Account", HttpMethod = "POST", ErrorMessage = "Email already exists. Please enter a different email.")]
        public string EmailAddress { get; set; }

        public int? CompanyID { get; set; } = 0;
        public List<SelectListItem> lst_Company { get; set; }

        [Required(ErrorMessage = "Username is required")]
        [Remote("IsUserNameExist", "Account", HttpMethod = "POST", ErrorMessage = "Username already exists. Please enter a different username.")]
        public string Username { get; set; }

        [Required(ErrorMessage = "Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        [System.ComponentModel.DataAnnotations.Compare("Password", ErrorMessage = "The Password and Confirmation Password does not match.")]
        public string ConfirmPassword { get; set; }
    }
}