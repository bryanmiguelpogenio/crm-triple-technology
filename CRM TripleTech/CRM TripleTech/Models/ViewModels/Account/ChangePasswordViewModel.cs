﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class ChangePasswordViewModel
    {
        [Required(ErrorMessage = "Id is required")]
        public int Id { get; set; }

        [Required(ErrorMessage = "Old Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Remote("IsChangePasswordCorrect", "Account", HttpMethod = "POST", ErrorMessage = "The password you entered is incorrect")]
        public string OldPassword { get; set; }

        [Required(ErrorMessage = "New Password is required")]
        [StringLength(50, ErrorMessage = "Password must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        public string NewPassword { get; set; }

        [Required(ErrorMessage = "Confirmation Password is required")]
        [System.ComponentModel.DataAnnotations.Compare("NewPassword", ErrorMessage = "The New Password and Confirmation Password does not match.")]
        public string ConfirmNewPassword { get; set; }
    }
}