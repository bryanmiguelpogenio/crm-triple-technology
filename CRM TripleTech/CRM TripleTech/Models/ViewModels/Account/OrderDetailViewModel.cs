﻿using CRM_TripleTech.Models.Entities;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class OrderDetailViewModel
    {
        public ORDERTRAN ordertran { get; set; }
    }
}