﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class AccountViewModel
    {
        [Required(ErrorMessage = "Username is required")]
        public string Username { get; set; }
        [Required(ErrorMessage = "Password is required")]
        [Remote("IsPasswordCorrect", "Account", AdditionalFields = "Username", HttpMethod = "POST", ErrorMessage = "Username or Password is incorrect.")]
        public string Password { get; set; }
    }
}