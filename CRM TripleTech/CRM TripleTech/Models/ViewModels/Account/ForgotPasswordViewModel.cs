﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Account
{
    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = "Please enter your email address")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [Remote("Change_IsEmailNotYetExist", "Account", HttpMethod = "POST", ErrorMessage = "Email is not yet registered. Please enter a different email.")]
        public string EmailAddress { get; set; }
    }
}