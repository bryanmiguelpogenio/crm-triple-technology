﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Company
{
    public class CompanyAddViewModel
    {
        [Required(ErrorMessage = "Type s required")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Code is required")]
        public string Code { get; set; }

        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        public string Address { get; set; }

        public string Description { get; set; }

        public List<SelectListItem> lst_Type { get; set; }

        public CompanyAddViewModel()
        {
            lst_Type = new List<SelectListItem>();

            lst_Type.Add(new SelectListItem
            {
                Text = Constant.COMPANY_TYPE_VENDOR,
                Value = Constant.COMPANY_TYPE_VENDOR
            });

            lst_Type.Add(new SelectListItem
            {
                Text = Constant.COMPANY_TYPE_NON_VENDOR,
                Value = Constant.COMPANY_TYPE_NON_VENDOR
            });
        }
    }
}