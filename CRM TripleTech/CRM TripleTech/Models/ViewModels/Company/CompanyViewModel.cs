﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Company
{
    public class CompanyViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public List<COMPANY> lst_Main { get; set; }
    }
}