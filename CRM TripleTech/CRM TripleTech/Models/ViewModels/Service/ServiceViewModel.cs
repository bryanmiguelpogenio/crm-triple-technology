﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Service
{
    public class ServiceViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }

        public List<SERVICE> lst_Main { get; set; }
    }
}