﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Stock
{
    public class StockReplenishViewModel
    {
        [Required(ErrorMessage = "ID is required")]
        public int InventoryID { get; set; }

        [Required(ErrorMessage = "Quantity is required")]
        public int Quantity { get; set; }

        [Required(ErrorMessage = "Company is required")]
        public string Company_ID { get; set; }

        public List<SelectListItem> lst_Company { get; set; }
    }
}