﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Inventory
{
    public class InventoryAddViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }

        public string Description { get; set; }

        [Required(ErrorMessage = "Quantity is required")]
        [RegularExpression(@"^[0-9]*$", ErrorMessage = "Invalid quantity format. must be a whole number.")]
        [Range(0, 9999999999999999, ErrorMessage = "Invalid quantity format. maximum of 16 digits")]
        public string Quantity { get; set; }

        [Required(ErrorMessage = "Price is required")]
        [RegularExpression(@"^[1-9]\d*(\.\d{1,2})?$", ErrorMessage = "Invalid pricing format. maximum of 2 decimal points.")]
        [Range(0, 9999999999999999, ErrorMessage = "Invalid pricing format. maximum of 16 digits")]
        public string Price { get; set; }

        [Required(ErrorMessage = "Category is required")]
        public int CategoryID { get; set; }
        public List<SelectListItem> lst_Category { get; set; }
    }
}