﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.Inventory
{
    public class InventoryViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        //public string Price { get; set; }

        public List<INVENTORY> lst_Main { get; set; }

        public int CategoryID { get; set; }
        public List<SelectListItem> drp_Category { get; set; }
    }
}