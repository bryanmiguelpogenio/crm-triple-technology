﻿using System;

namespace CRM_TripleTech.Models.ViewModels.Product
{
    public class ServiceSchedViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Location { get; set; }
        public DateTime? SchedDate { get; set; }
    }
}