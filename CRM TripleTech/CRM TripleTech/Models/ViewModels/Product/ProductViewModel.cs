﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Product
{
    public class ProductViewModel
    {
        public List<INVENTORY> lst_Motor { get; set; }
        public List<INVENTORY> lst_Pump { get; set; }
        public List<INVENTORY> lst_Controller { get; set; }
        public List<SERVICE> lst_Service { get; set; }
    }
}