﻿using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.ServiceTran
{
    public class ServiceTranDetailViewModel
    {
        public List<SelectListItem> drp_User { get; set; }
        public List<SelectListItem> drp_Priority { get; set; }

        public ServiceTranModel Main { get; set; }
    }
}