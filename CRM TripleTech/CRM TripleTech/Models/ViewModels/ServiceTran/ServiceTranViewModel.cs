﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.ServiceTran
{
    public class ServiceTranViewModel
    {
        public string ID { get; set; }

        public string UserID { get; set; }
        public List<SelectListItem> drp_User { get; set; }

        public string ServiceID { get; set; }
        public List<SelectListItem> drp_Service { get; set; }

        public List<ServiceTranModel> lst_Main { get; set; }
    }

    public class ServiceTranModel
    {
        public int Id { get; set; }
        public string ServiceName { get; set; }
        public string CustomerName { get; set; }
        public string Company { get; set; }
        public string PriorityLevel { get; set; }
        public string Location { get; set; }
        public string Status { get; set; }

        public bool HasAssignedEmployee { get; set; }

        public int ServiceSchedID { get; set; }
        [Required(ErrorMessage = "Assigned user is required")]
        public int AssignedUser { get; set; }

        [Required(ErrorMessage = "Date scheduled is required")]
        public DateTime? DateScheduled { get; set; }
        public string ServiceSchedStatus { get; set; }
    }
}