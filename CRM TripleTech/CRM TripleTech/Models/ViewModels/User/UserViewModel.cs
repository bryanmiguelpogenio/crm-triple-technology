﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Models.ViewModels.User
{
    public class UserViewModel
    {
        public string ID { get; set; }

        public string CustomerName { get; set; }
        public List<SelectListItem> drp_User { get; set; }

        public int RoleID { get; set; }
        public List<SelectListItem> drp_Role { get; set; }

        public int CompanyID { get; set; }
        public List<SelectListItem> drp_Company { get; set; }

        public List<USER> lst_Main { get; set; }
    }
}