﻿using System.ComponentModel.DataAnnotations;

namespace CRM_TripleTech.Models.ViewModels.Category
{
    public class CategoryAddViewModel
    {
        [Required(ErrorMessage = "Name is required")]
        public string Name { get; set; }
        public string Description { get; set; }
    }
}