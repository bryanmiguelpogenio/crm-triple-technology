﻿using CRM_TripleTech.Models.Entities;
using System.Collections.Generic;

namespace CRM_TripleTech.Models.ViewModels.Category
{
    public class CategoryViewModel
    {
        public string ID { get; set; }
        public string Name { get; set; }
        public List<CATEGORY> lst_Main { get; set; }

        //public CategoryAddViewModel Model_Add { get; set; }
    }
}