namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("INVENTORY")]
    public partial class INVENTORY
    {
        public int ID { get; set; }

        [ForeignKey("CATEGORY")]
        public int CATEGORY_ID { get; set; }

        [StringLength(100)]
        public string NAME { get; set; }

        public string DESCRIPTION { get; set; }

        public int QUANTITY { get; set; }

        public double PRICE { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual CATEGORY CATEGORY { get; set; }
        public virtual ICollection<ORDERTRANDETAIL> ORDERTRANDETAILS { get; set; }
    }
}
