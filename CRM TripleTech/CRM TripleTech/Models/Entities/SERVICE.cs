namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SERVICE")]
    public partial class SERVICE
    {
        public int ID { get; set; }

        [StringLength(100)]
        public string NAME { get; set; }

        public string DESCRIPTION { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual ICollection<SERVICETRAN> SERVICETRANS { get; set; }
        //public virtual ICollection<USERSERVICESCHED> USERSERVICESCHEDS { get; set; }
    }
}
