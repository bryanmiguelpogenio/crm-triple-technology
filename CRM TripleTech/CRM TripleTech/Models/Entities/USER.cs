namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;
    using System.Web.Mvc;

    [Table("USER")]
    public partial class USER
    {
        public int ID { get; set; }

        [ForeignKey("ROLE")]
        public int? ROLE_ID { get; set; }

        [ForeignKey("COMPANY")]
        public int? COMPANY_ID { get; set; }

        [StringLength(50)]public string FIRSTNAME { get; set; }

        [StringLength(50)]public string MIDDLENAME { get; set; }

        [StringLength(50)]public string LASTNAME { get; set; }

        [StringLength(50)]
        public string CONTACTNO { get; set; }

        [StringLength(50)]public string EMAILADDRESS { get; set; }

        [StringLength(50)]
        public string TELEPHONENO { get; set; }

        [StringLength(50)]public string USERNAME { get; set; }
        
        public string PASSWORD { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual ROLE ROLE { get; set; }
        public virtual COMPANY COMPANY { get; set; }

        //public virtual ICollection<FEEDBACKTRAN> ASCLIENTS { get; set; }
        //public virtual ICollection<FEEDBACKTRAN> ASADMINS { get; set; }
        public virtual ICollection<ORDERTRAN> ASORDERUSERS { get; set; }
        public virtual ICollection<SERVICETRAN> ASSERVICEUSERS { get; set; }
        public virtual ICollection<USERSERVICESCHED> ASSERVICESCHEDUSERS { get; set; }
        public virtual ICollection<ADDRESS> ADDRESSES { get; set; }
    }
}
