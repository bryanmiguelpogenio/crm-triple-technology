namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ADDRESS")]
    public partial class ADDRESS
    {
        public int ID { get; set; }

        [ForeignKey("USER")]
        public int USER_ID { get; set; }

        public string ADDRESSNAME { get; set; }

        [StringLength(50)]
        public string BARANGAY { get; set; }

        [StringLength(50)]
        public string MUNICIPALITY { get; set; }

        [StringLength(5)]
        public string POSTALCODE { get; set; }

        [StringLength(50)]
        public string LOCALITY { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual USER USER { get; set; }
    }
}
