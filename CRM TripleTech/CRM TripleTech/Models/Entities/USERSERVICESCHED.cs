namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("USERSERVICESCHED")]
    public partial class USERSERVICESCHED
    {
        public int ID { get; set; }

        [ForeignKey("USER")]
        public int USER_ID { get; set; }

        [ForeignKey("SERVICETRAN")]
        public int SERVICE_ID { get; set; }

        public DateTime? DATESCHEDULED { get; set; }

        [StringLength(50)]
        public string STATUS { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual USER USER { get; set; }
        public virtual SERVICETRAN SERVICETRAN { get; set; }
    }
}
