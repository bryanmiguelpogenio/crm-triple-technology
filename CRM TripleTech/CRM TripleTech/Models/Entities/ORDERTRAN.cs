namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ORDERTRAN")]
    public partial class ORDERTRAN
    {
        public int ID { get; set; }

        [StringLength(50)]
        [Required]
        public string ORDERNO { get; set; }

        [ForeignKey("USER")]
        public int USER_ID { get; set; }

        //[ForeignKey("ORDERTRANDETAIL")]
        //public int ORDERTRANDETAIL_ID { get; set; }

        [StringLength(50)]
        public string PRIORITYLEVEL { get; set; }

        //[StringLength(50)]
        public string LOCATION { get; set; }

        [StringLength(50)]
        public string STATUS { get; set; }

        public double TOTALPRICE { get; set; }

        public DateTime? DATEORDERED { get; set; }

        public DateTime? DATEDELIVERED { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual USER USER { get; set; }
        public virtual ICollection<ORDERTRANDETAIL> ORDERTRANDETAIL { get; set; }
    }
}
