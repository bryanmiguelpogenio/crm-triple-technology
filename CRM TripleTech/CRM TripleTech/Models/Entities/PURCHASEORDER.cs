﻿namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("PURCHASEORDER")]
    public partial class PURCHASEORDER
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string PURCHASENO { get; set; }

        [ForeignKey("COMPANY")]
        public int COMPANY_ID { get; set; }

        [ForeignKey("INVENTORY")]
        public int INVENTORY_ID { get; set; }

        public int QUANTITY { get; set; }

        public double AMOUNT { get; set; }

        public DateTime? DATEPURCHASED { get; set; }

        [ForeignKey("USER")]
        public int BUYER_ID {get;set;}

        public DateTime? DATEUPDATED { get; set; }

        public string STATUS { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual USER USER { get; set; }
        public virtual COMPANY COMPANY { get; set; }
        public virtual INVENTORY INVENTORY { get; set; }

    }
}