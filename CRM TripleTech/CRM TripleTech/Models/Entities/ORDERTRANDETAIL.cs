namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ORDERTRANDETAIL")]
    public partial class ORDERTRANDETAIL
    {
        public int ID { get; set; }

        [ForeignKey("ORDERTRAN")]
        public int ORDERTRAN_ID { get; set; }

        [ForeignKey("INVENTORY")]
        public int INVENTORY_ID { get; set; }

        public int QUANTITY { get; set; }

        [StringLength(50)]
        public string STATUS { get; set; }

        public double PRICE { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual INVENTORY INVENTORY { get; set; }
        public virtual ORDERTRAN ORDERTRAN { get; set; }
    }
}
