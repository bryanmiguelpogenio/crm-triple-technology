namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FEEDBACKTRAN")]
    public partial class FEEDBACKTRAN
    {
        public int ID { get; set; }

        //[ForeignKey("CLIENT")]
        public int CLIENT_ID { get; set; }

        [StringLength(50)]
        public string SUBJECT { get; set; }

        public string USER_COMPLAINT { get; set; }

        //[ForeignKey("ADMIN")]
        public int ADMIN_ID { get; set; }

        public string ADMIN_RESPONSE { get; set; }

        [StringLength(50)]
        public string PRIORITY { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public bool ISDONE { get; set; }

        [NotMapped]
        public virtual USER CLIENT { get; set; }
        [NotMapped]
        public virtual USER ADMIN { get; set; }
    }
}
