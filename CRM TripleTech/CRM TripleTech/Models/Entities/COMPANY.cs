namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("COMPANY")]
    public partial class COMPANY
    {
        public int ID { get; set; }

        [StringLength(50)]
        public string TYPE { get; set; }

        [StringLength(100)]
        public string CODE { get; set; }

        [StringLength(100)]
        public string NAME { get; set; }

        public string DESCRIPTION { get; set; }

        public string ADDRESS { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual ICollection<USER> USERS { get; set; }
    }
}
