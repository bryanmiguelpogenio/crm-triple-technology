namespace CRM_TripleTech.Models.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SERVICETRAN")]
    public partial class SERVICETRAN
    {
        public int ID { get; set; }

        [ForeignKey("USER")]
        public int USER_ID { get; set; }

        [ForeignKey("SERVICE")]
        public int SERVICE_ID { get; set; }

        [StringLength(50)]
        public string PRIORITYLEVEL { get; set; }

        public string LOCATION { get; set; }

        [StringLength(50)]
        public string STATUS { get; set; }

        public DateTime? DATECOMPLETED { get; set; }

        public DateTime? DATEUPDATED { get; set; }

        public int LASTUPDATEDBY { get; set; }

        public virtual USER USER { get; set; }
        public virtual SERVICE SERVICE { get; set; }
        public virtual ICollection<USERSERVICESCHED> USERSERVICESCHEDS { get; set; }
    }
}
