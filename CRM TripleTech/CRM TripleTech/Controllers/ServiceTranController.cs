﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.ViewModels.ServiceTran;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class ServiceTranController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ServiceTranController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: ServiceTran
        public ActionResult Index(ServiceTranViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = new List<ServiceTranModel>();
            var servicetran = unitOfWork.ServiceTran.SearchFilter(model, m => m.STATUS != Constant.SERVICE_STATUS_COMPLETED);

            foreach (var item in servicetran)
            {
                var userservicetran = unitOfWork.UserServiceTran.getEntityByServiceTranID(item.ID);

                model.lst_Main.Add(new ServiceTranModel
                {
                    Id = item.ID,
                    ServiceName = item.SERVICE.NAME,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    Location = item.LOCATION,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Status = item.STATUS,
                    HasAssignedEmployee = userservicetran != null && userservicetran.USER_ID != Constant.NO_USER_ID ? true : false
                });
            }

            model.drp_User = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Service = unitOfWork.Service.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (!id.HasValue || id.Value == Constant.NO_ORDERTRAN_ID) return RedirectToAction("Index");

            ServiceTranDetailViewModel model = new ServiceTranDetailViewModel();
            
                model.drp_User = unitOfWork.User.GetAll().Where(m => m.ROLE.ID == Constant.ROLE_TECHNICIAN_ID).Select(m => new SelectListItem
                {
                    Text = m.FIRSTNAME + " " + m.LASTNAME,
                    Value = m.ID.ToString()
                }).ToList();

                var servicetran = unitOfWork.ServiceTran.Get(id.Value);
                var userservicetran = unitOfWork.UserServiceTran.getEntityByServiceTranID(id.Value);

                model.Main = new ServiceTranModel();

                model.Main.Id = servicetran.ID;
                model.Main.ServiceName = servicetran.SERVICE.NAME;
                model.Main.CustomerName = servicetran.USER.FIRSTNAME + " " + servicetran.USER.LASTNAME;
                model.Main.Company = servicetran.USER.COMPANY_ID != 0 ? servicetran.USER.COMPANY.NAME : string.Empty;
                model.Main.Location = servicetran.LOCATION;
                model.Main.PriorityLevel = servicetran.PRIORITYLEVEL;
                model.Main.Status = servicetran.STATUS;
                model.Main.HasAssignedEmployee = userservicetran != null && userservicetran.USER_ID != Constant.NO_USER_ID ? true : false;


                model.Main.ServiceSchedID = userservicetran.ID;
                model.Main.AssignedUser = userservicetran.USER_ID;
                model.Main.DateScheduled = userservicetran.DATESCHEDULED;
                model.Main.ServiceSchedStatus = userservicetran.STATUS;

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(ServiceTranDetailViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                    var userservicetran = unitOfWork.UserServiceTran.Get(model.Main.ServiceSchedID);

                    userservicetran.USER_ID = model.Main.AssignedUser;
                    userservicetran.DATESCHEDULED = model.Main.DateScheduled;
                    userservicetran.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                    userservicetran.DATEUPDATED = DateTime.UtcNow;

                    unitOfWork.UserServiceTran.Update(userservicetran);
                    unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            return View();
        }

        public ActionResult UpdateStatus(int id, string transaction)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");
            
                var servicetran = unitOfWork.ServiceTran.Get(id);

                switch (transaction)
                {
                    case Constant.SERVICE_STATUS_COMPLETED:
                        servicetran.STATUS = Constant.SERVICE_STATUS_COMPLETED;
                        break;
                    case Constant.SERVICE_STATUS_PROCESSING:
                        servicetran.STATUS = Constant.SERVICE_STATUS_PROCESSING;
                        break;
                }

                servicetran.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                servicetran.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.ServiceTran.Update(servicetran);
                unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}