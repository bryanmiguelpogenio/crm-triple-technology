﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Autofac;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;

namespace CRM_TripleTech.Controllers
{
    public class HomeController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public HomeController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Home
        public ActionResult Index()
        {
            ClientFeedbackViewModel model = new ClientFeedbackViewModel();

            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value != Constant.NO_USER_ID)
            {
                    var user = unitOfWork.User.Get(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);
                    model.Name = user.FIRSTNAME + " " + user.LASTNAME;
            }

            return View(model);
        }
    }
}