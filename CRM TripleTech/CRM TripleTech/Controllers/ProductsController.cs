﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Product;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class ProductsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ProductsController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Products
        public ActionResult Index()
        {
            ProductViewModel model = new ProductViewModel();

                var entities = unitOfWork.Inventory.Find(m => m.QUANTITY != 0);

                model.lst_Motor = unitOfWork.Inventory.Find(m => m.CATEGORY_ID != Constant.NO_CATEGORY_ID 
                && m.CATEGORY.NAME.ToUpper().Contains(Constant.CATEGORY_MOTORS)).ToList();

                model.lst_Pump = unitOfWork.Inventory.Find(m => m.CATEGORY_ID != Constant.NO_CATEGORY_ID
                && m.CATEGORY.NAME.ToUpper().Contains(Constant.CATEGORY_PUMPS)).ToList();

                model.lst_Controller = unitOfWork.Inventory.Find(m => m.CATEGORY_ID != Constant.NO_CATEGORY_ID
                && m.CATEGORY.NAME.ToUpper().Contains(Constant.CATEGORY_CONTROLLERS)).ToList();

                model.lst_Service = unitOfWork.Service.GetAll().ToList();

            return View(model);
        }

        public ActionResult AddToCart(int id, int quantity)
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index", "Account");

            int userid = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

            //--> Check if user has an active cart in OrderTran
            var ordertran = unitOfWork.OrderTran.getActiveEntityByUserID(userid);

            if (ordertran == null)
            {
                //--> If not, create a new active order transaction
                ordertran = new ORDERTRAN();
                ordertran.ORDERNO = Constant.DEFAULT_CODE;
                ordertran.USER_ID = userid;
                ordertran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_LOW;
                ordertran.STATUS = Constant.ORDERTRAN_STATUS_ACTIVE;
                ordertran.DATEUPDATED = DateTime.UtcNow;
                ordertran.LASTUPDATEDBY = userid;

                unitOfWork.OrderTran.Add(ordertran);
            }

            //--> Check if inventory item exists and if it has sufficient stocks left
            var inventory = unitOfWork.Inventory.Get(id);

            if (inventory != null)  //&& unitOfWork.Inventory.IsStockHasMoreQuantity(id, quantity)
            {
                //--> If exists, create a new transaction details
                unitOfWork.OrderTranDetail.Add(new ORDERTRANDETAIL
                {
                    ORDERTRAN_ID = ordertran.ID,
                    INVENTORY_ID = inventory.ID,
                    QUANTITY = quantity,
                    STATUS = Constant.ORDERTRAN_STATUS_ACTIVE,
                    PRICE = quantity * inventory.PRICE,
                    DATEUPDATED = DateTime.UtcNow,
                    LASTUPDATEDBY = userid
                });

                unitOfWork.SaveChanges();

                //--> Generate OrderNo
                ordertran.ORDERNO = StaticMethods.CreateOrderNo(ordertran.ID.ToString());
                unitOfWork.OrderTran.Update(ordertran);

                unitOfWork.SaveChanges();
            }

            return Json(new { message = Constant.ALERT_ITEM_ADDED }, JsonRequestBehavior.AllowGet);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult PuchaseService(ServiceSchedViewModel model)
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                    var service = unitOfWork.Service.Get(model.Id);

                    var servicetran = new SERVICETRAN();

                    servicetran.SERVICE_ID = model.Id;
                        servicetran.USER_ID = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                        servicetran.LOCATION = model.Location;
                        servicetran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_LOW;
                        servicetran.STATUS = Constant.SERVICE_STATUS_ACTIVE;
                        servicetran.DATEUPDATED = DateTime.UtcNow;
                        servicetran.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

                    unitOfWork.ServiceTran.Add(servicetran);

                    var userservicesched = new USERSERVICESCHED();

                    userservicesched.SERVICE_ID = servicetran.ID;
                    userservicesched.DATESCHEDULED = model.SchedDate;
                    userservicesched.USER_ID = Constant.NO_USER_ID;
                    userservicesched.STATUS = Constant.SERVICE_STATUS_ACTIVE;
                    userservicesched.DATEUPDATED = DateTime.UtcNow;
                    userservicesched.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

                    unitOfWork.UserServiceTran.Add(userservicesched);

                    unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            return View();
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_SCHED:
                    ServiceSchedViewModel schedViewModel = new ServiceSchedViewModel();
                    schedViewModel.Id = StaticMethods.TryParseStringToInt(id).Value;
                    
                        var service = unitOfWork.Service.Get(schedViewModel.Id);
                        schedViewModel.Name = service.NAME;

                    paramContent = RenderPartialViewToString("_ModalCategorySched", schedViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}