﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class FeedbackController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public FeedbackController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Feedback
        public ActionResult Index(FeedbackViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.drp_Client = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            var feedbacktransaction = unitOfWork.FeedbackTran.SearchFilter(model, m => m.ISDONE == false).ToList();

            model.lst_Main = new List<FeedbackModel>();
            foreach (var item in feedbacktransaction)
            {
                item.CLIENT = unitOfWork.User.Get(item.CLIENT_ID);

                if (item.CLIENT != null)
                {
                    model.lst_Main.Add(new FeedbackModel
                    {
                        Id = item.ID.ToString(),
                        Subject = item.SUBJECT,
                        Status = item.PRIORITY,
                        CustomerName = item.CLIENT_ID != Constant.NO_USER_ID ? item.CLIENT.FIRSTNAME + " " + item.CLIENT.LASTNAME : string.Empty,
                        CompanyName = item.CLIENT.COMPANY_ID.HasValue & item.CLIENT.COMPANY_ID != 0 ? item.CLIENT.COMPANY.NAME : string.Empty,
                        HasReplied = !string.IsNullOrEmpty(item.ADMIN_RESPONSE) ? true : false
                    });
                }
            }

            return View(model);
        }


        //--> Homepage
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ClientFeedback(ClientFeedbackViewModel model)
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                    unitOfWork.FeedbackTran.Add(new FEEDBACKTRAN
                    {
                        CLIENT_ID = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        SUBJECT = model.Subject,
                        USER_COMPLAINT = model.Message,
                        PRIORITY = Constant.PRIORITY_LEVEL_LOW,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow
                    });

                    unitOfWork.SaveChanges();

                return RedirectToAction("Index", "Home");
            }

            return View();
        }

        public ActionResult Details(int id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var model = new AdminFeedbackViewModel();
            var feedbacktran = unitOfWork.FeedbackTran.Get(id);
            var user = unitOfWork.User.Get(feedbacktran.CLIENT_ID);

            model.Id = feedbacktran.ID;
            model.Subject = feedbacktran.SUBJECT;
            model.ClientMessage = feedbacktran.USER_COMPLAINT;
            model.CustomerName = user.FIRSTNAME + " " + user.LASTNAME;
            model.CompanyName = user.COMPANY_ID != 0 ? user.COMPANY.NAME : string.Empty;
            model.AdminMessage = string.Empty;

            return View(model);
        }

        public ActionResult CompleteFeedback(int id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var model = unitOfWork.FeedbackTran.Get(id);
            model.ISDONE = true;

            unitOfWork.FeedbackTran.Update(model);
            unitOfWork.SaveChanges();

            return RedirectToAction("Index", "Feedback");
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult AdminFeedback(AdminFeedbackViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                    var feedbacktran = unitOfWork.FeedbackTran.Get(model.Id);

                    feedbacktran.ADMIN_ID = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                    feedbacktran.ADMIN_RESPONSE = model.AdminMessage;
                    feedbacktran.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                    feedbacktran.DATEUPDATED = DateTime.UtcNow;

                    unitOfWork.FeedbackTran.Update(feedbacktran);
                    unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            return View();

        }
    }
}