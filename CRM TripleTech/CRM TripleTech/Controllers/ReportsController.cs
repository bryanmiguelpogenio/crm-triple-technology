﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Report;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class ReportsController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        
        public ReportsController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }
        // GET: Reports
        public ActionResult PurchaseOrder()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            List<PurchaseOrderReportViewModel> model = getPurchaseOrder();
            return View(model);
        }

        public ActionResult SalesOrder()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            List<SalesOrderReportViewModel> model = getSalesOrder();

            return View(model);
        }

        public ActionResult StatementOfAccount()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            List<OrderListStatementOfAccReportViewModel> model = GetStatementOfAccList();

            return View(model);
        }

        public ActionResult SupplierList()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            List<SupplierReportViewModel> model = GetSupplierList();

            return View(model);
        }

        public ActionResult InventorySummary()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            List<InventoryReportViewModel> model = GetInventorySummary();

            return View(model);
        }

        public ActionResult ScheduleSummary()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            List<ScheduleReportViewModel> model = GetScheduleSummary();

            return View(model);
        }

        #region LIST
        public List<PurchaseOrderReportViewModel> getPurchaseOrder()
        {
            List<PurchaseOrderReportViewModel> model = new List<PurchaseOrderReportViewModel>();

            var purchaseorder = unitOfWork.PurchaseOrder.GetAll();

            foreach(var item in purchaseorder)
            {
                model.Add(new PurchaseOrderReportViewModel
                {
                    PurchaseOrderID = item.PURCHASENO,
                    VendorCode = item.COMPANY != null ? item.COMPANY.CODE : string.Empty,
                    CompanyName = item.COMPANY != null ? item.COMPANY.NAME : string.Empty,
                    DateOrdered = item.DATEPURCHASED.HasValue ? item.DATEPURCHASED.Value.ToShortDateString() : string.Empty,
                    BuyerName = item.USER != null ? item.USER.FIRSTNAME + " " + item.USER.LASTNAME : string.Empty,
                    FreightCharge = "0.00",
                    SalesTax = "0.00",
                    Amount = item.AMOUNT.ToString()

                });
            }

            return model;
        }

        public List<SalesOrderReportViewModel> getSalesOrder()
        {
            List<SalesOrderReportViewModel> model = new List<SalesOrderReportViewModel>();

            var ordertrans = unitOfWork.OrderTran.GetAll();
            foreach (var item in ordertrans)
            {
                var admin = unitOfWork.User.Get(item.LASTUPDATEDBY);

                model.Add(new SalesOrderReportViewModel
                {
                    SalesOrderID = item.ORDERNO, //--> Add Last two characters of a year
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    CompanyName = item.USER.COMPANY != null ? item.USER.COMPANY.NAME : string.Empty,
                    DateOrdered = item.DATEORDERED.HasValue ? item.DATEORDERED.Value.ToShortDateString() : string.Empty, //--> Add DateOrdered on OrderTransactionTable
                    SalesPerson = admin.FIRSTNAME + " " + admin.LASTNAME,
                    PayCode = "NA",
                    SalesTax = "0.00",
                    Amount = item.TOTALPRICE.ToString()
                });
            };

            return model;
        }

        public List<OrderListStatementOfAccReportViewModel> GetStatementOfAccList()
        {
            List<OrderListStatementOfAccReportViewModel> model = new List<OrderListStatementOfAccReportViewModel>();

            var ordertransactions = unitOfWork.OrderTran.GetAll();

            foreach (var item in ordertransactions)
            {
                model.Add(new OrderListStatementOfAccReportViewModel
                {
                    Id = item.ID,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Location = item.LOCATION,
                    Status = item.STATUS,
                    TotalPrice = item.TOTALPRICE
                });
            }

            return model;
        }

        public List<SupplierReportViewModel> GetSupplierList()
        {
            List<SupplierReportViewModel> model = new List<SupplierReportViewModel>();

            var companies = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_VENDOR);
            foreach (var item in companies)
            {
                model.Add(new SupplierReportViewModel
                {
                    SupplierCode = item.CODE,
                    Name = item.NAME,
                    Address = item.ADDRESS,
                });
            }

            return model;
        }

        public List<InventoryReportViewModel> GetInventorySummary()
        {
            List<InventoryReportViewModel> model = new List<InventoryReportViewModel>();

            var inventory = unitOfWork.Inventory.GetAll();
            foreach (var item in inventory)
            {
                model.Add(new InventoryReportViewModel
                {
                    InventoryID = item.ID.ToString(),
                    Name = item.NAME,
                    Category = item.CATEGORY.NAME,
                    StockQuantity = item.QUANTITY.ToString(),
                    //ShippedQuantity = "0.00",
                    //SoldQuantity = "0.00",
                    TotalCost = (item.QUANTITY * item.PRICE).ToString()
                });
            }

            return model;
        }

        public List<ScheduleReportViewModel> GetScheduleSummary()
        {
            List<ScheduleReportViewModel> model = new List<ScheduleReportViewModel>();

            var servicetran = unitOfWork.ServiceTran.GetAll();
            foreach (var item in servicetran)
            {
                var servicesched = item.USERSERVICESCHEDS.LastOrDefault();
                model.Add(new ScheduleReportViewModel
                {
                    ID = item.ID.ToString(),
                    Name = item.SERVICE != null ? item.SERVICE.NAME : string.Empty,
                    Client = item.USER != null ? item.USER.FIRSTNAME + " " + item.USER.LASTNAME : string.Empty,
                    Company = item.USER.COMPANY != null ? item.USER.COMPANY.NAME : string.Empty,
                    Assigned = servicesched.USER != null ? servicesched.USER.FIRSTNAME + " " + servicesched.USER.LASTNAME : string.Empty,
                    DateScheduled = servicesched.DATESCHEDULED.HasValue ? servicesched.DATESCHEDULED.Value.ToLocalTime().ToShortDateString() : string.Empty,
                    Status = item.STATUS,
                });
            }

            return model;
        }
        #endregion

        //--> 1 - Purchase Order
        //--> 2 - Sales Order
        //--> 3 - Statement of Account
        //--> 4 - Supplier List
        //--> 5 - Inventory Summary
        //--> 6 - Schedule Summary
        public FileResult ExportOrder(int type, int orderId = 0)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                //StringReader sr = new StringReader("");
                DateTime dateTime = DateTime.Now;
                Document doc = new Document(PageSize.A4.Rotate());
                doc.SetMargins(10f, 10f, 10f, 10f);

                PdfWriter.GetInstance(doc, stream).CloseStream = false;
                doc.Open();

                if (type== 1)
                {
                    //Create PDF Table
                    PdfPTable tableLayout = new PdfPTable(8);

                    string[] headers = { "PO #", "Vendor Code", "Company", "Date Ordered", "Buyer", "Freight Charge", "Sales Tax", "Amount" };
                    float[] width = { 25, 50, 50, 25, 50, 25, 25, 45 };

                    List<PurchaseOrderReportViewModel> model = getPurchaseOrder();
 
                    //Add Content to PDF   
                    doc.Add(AddTableToPDF(tableLayout, headers, width, model, "Purchase Order Summary Report"));

                }
                else if (type == 2)
                {
                    //Create PDF Table
                    PdfPTable tableLayout = new PdfPTable(8);

                    string[] headers = { "SO #", "Customer Name", "Company", "Date Ordered", "Sales Person", "Pay Code", "Sales Tax", "Amount" };
                    float[] width = { 25, 50, 50, 25, 50, 25, 25, 45 };

                    List<SalesOrderReportViewModel> model = getSalesOrder();

                    //Add Content to PDF   
                    doc.Add(AddTableToPDF(tableLayout, headers, width, model, "Sales Order Summary Report"));
                }
                else if (type == 3 && orderId != 0)
                {
                    PdfPTable tableLayout = new PdfPTable(6);

                    string[] headers = { "No", "Category", "Name", "Quantity", "Price", "Amount" };
                    float[] width = { 25, 50, 50, 50, 25, 25 };

                    var ordertran = unitOfWork.OrderTran.Get(orderId);

                    StatementOfAccReportViewModel model = new StatementOfAccReportViewModel();

                    model.CustomerNo = ordertran.USER_ID.ToString();
                    model.CustomerName = ordertran.USER != null ? ordertran.USER.FIRSTNAME + " " + ordertran.USER.LASTNAME : string.Empty;
                    model.Company = ordertran.USER != null && ordertran.USER.COMPANY != null ? ordertran.USER.COMPANY.NAME : string.Empty;
                    model.DeliveryType = Constant.DELIVERY_TYPE;
                    model.DateOrdered = ordertran.DATEORDERED.HasValue ? ordertran.DATEORDERED.Value.ToLocalTime().ToString() : string.Empty;
                    model.DatePrinted = DateTime.UtcNow.ToLocalTime().ToString();
                    model.TotalAmount = ordertran.TOTALPRICE.ToString();

                    var ordertrandetails = ordertran.ORDERTRANDETAIL.ToList();

                    model.lst_Main = new List<SOADetailModel>();
                    foreach (var item in ordertrandetails)
                    {
                        var price = item.PRICE / item.QUANTITY;

                        model.lst_Main.Add(new SOADetailModel
                        {
                            Id = item.ID,
                            CategoryName = item.INVENTORY.CATEGORY.NAME,
                            Name = item.INVENTORY.NAME,
                            Price = price,
                            Quantity = item.QUANTITY,
                            Amount = item.PRICE
                        });
                    }

                    //Add Content to PDF  
                    //doc.Add(AddTableContentToPDF(tableLayout, width, model, "Statement of Account Report"));
                    doc.Add(AddTableToPDF(tableLayout, headers, width, model.lst_Main, "Statement of Account Report", model));
                }
                else if (type == 4)
                {
                    //Create PDF Table
                    PdfPTable tableLayout = new PdfPTable(3);

                    string[] headers = { "Supplier Code", "Name", "Address" };
                    float[] width = { 25, 50, 50};

                    List<SupplierReportViewModel> model = GetSupplierList();

                    //Add Content to PDF   
                    doc.Add(AddTableToPDF(tableLayout, headers, width, model, "Supplier List"));
                }
                else if (type == 5)
                {
                    //Create PDF Table
                    PdfPTable tableLayout = new PdfPTable(7);

                    //string[] headers = { "No", "Name", "Category", "Stock Quantity", "Shipped", "Sold", "Total Cost" };
                    string[] headers = { "No", "Name", "Category", "Stock Quantity", "Total Cost" };
                    float[] width = { 20, 50, 50, 25, 25, 25, 50 };

                    List<InventoryReportViewModel> model = GetInventorySummary();

                    //Add Content to PDF   
                    doc.Add(AddTableToPDF(tableLayout, headers, width, model, "Inventory Summary Report"));
                }
                else if (type == 6)
                {
                    PdfPTable tableLayout = new PdfPTable(7);

                    //string[] headers = { "No", "Name", "Category", "Stock Quantity", "Shipped", "Sold", "Total Cost" };
                    string[] headers = { "No", "Service", "Client", "Company", "Assigned", "DateScheduled", "Status" };
                    float[] width = { 20, 50, 50, 50, 50, 25, 25 };

                    List<ScheduleReportViewModel> model = GetScheduleSummary();

                    //Add Content to PDF   
                    doc.Add(AddTableToPDF(tableLayout, headers, width, model, "Schedule Summary Report"));
                }

                // Closing the document  
                doc.Close();

                byte[] byteInfo = stream.ToArray();
                stream.Write(byteInfo, 0, byteInfo.Length);
                stream.Position = 0;

                return File(stream.ToArray(), "application/pdf", "ExportPDF.pdf");
            }
        }


        protected PdfPTable AddTableToPDF<TEntity>(PdfPTable tableLayout, string[] headerval, float[] headerwidth, List<TEntity> model, string title, StatementOfAccReportViewModel content = null)
            where TEntity : class
        {

            tableLayout.SetWidths(headerwidth); //Set the pdf headers  
            tableLayout.WidthPercentage = 100; //Set the PDF File witdh percentage  
            tableLayout.HeaderRows = 1;

            //Add Title to the PDF file at the top  
            tableLayout.AddCell(new PdfPCell(new Phrase(title, new Font(Font.FontFamily.HELVETICA, 20, Font.NORMAL, new BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 0,
                HorizontalAlignment = Element.ALIGN_CENTER
            });
            tableLayout.AddCell(new PdfPCell(new Phrase("As of " + DateTime.Now.ToString(), new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 10,
                FixedHeight = 30,
                HorizontalAlignment = Element.ALIGN_CENTER
            });

            if (content != null)
            {
                AddCellToBodyNoBorder(tableLayout, "Customer #: " + content.CustomerNo, 3);
                AddCellToBodyNoBorder(tableLayout, "Customer Name: " + content.CustomerName, 3);
                AddCellToBodyNoBorder(tableLayout, "Company: " + content.Company, 3);
                AddCellToBodyNoBorder(tableLayout, "Total Amount: ₱ " + content.TotalAmount, 3);
                AddCellToBodyNoBorder(tableLayout, "Delivery Type: " + content.DeliveryType, 3);
                AddCellToBodyNoBorder(tableLayout, "Date Ordered: " + content.DateOrdered, 3);

                tableLayout.AddCell(new PdfPCell(new Phrase(" ", new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(0, 0, 0))))
                {
                    Colspan = 12,
                    Border = 0,
                    PaddingBottom = 15,
                    FixedHeight = 30,
                    HorizontalAlignment = Element.ALIGN_CENTER
                });

                //AddCellToBodyNoBorder(tableLayout, "Date Printed: " + content.DatePrinted, 3);
            }

            //--> Add header  
            foreach (var value in headerval)
            {
                AddCellToHeader(tableLayout, value);
            }

            //--> Add body  
            foreach (var item in model)
            {
                var properties = item.GetType().GetProperties();
                foreach (var prop in properties)
                {
                    AddCellToBody(tableLayout, prop.GetValue(item, null).ToString());
                }
            }

            return tableLayout;
        }

        protected PdfPTable AddTableContentToPDF(PdfPTable tableLayout, float[] headerwidth, StatementOfAccReportViewModel model, string title)
        {
            tableLayout.SetWidths(headerwidth); //Set the pdf headers  
            tableLayout.WidthPercentage = 100; //Set the PDF File witdh percentage  
            tableLayout.HeaderRows = 1;

            tableLayout.AddCell(new PdfPCell(new Phrase(title, new Font(Font.FontFamily.HELVETICA, 20, Font.NORMAL, new BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 0,
                HorizontalAlignment = Element.ALIGN_CENTER
            });
            tableLayout.AddCell(new PdfPCell(new Phrase("As of " + DateTime.Now.ToString(), new Font(Font.FontFamily.HELVETICA, 8, Font.NORMAL, new BaseColor(0, 0, 0))))
            {
                Colspan = 12,
                Border = 0,
                PaddingBottom = 10,
                FixedHeight = 30,
                HorizontalAlignment = Element.ALIGN_CENTER
            });

            AddCellToBody(tableLayout, "Customer #: " + model.CustomerNo, 3);
            AddCellToBody(tableLayout,"Customer Name: " + model.CustomerName, 3);
            AddCellToBody(tableLayout, "Company: " + model.Company, 3);
            AddCellToBody(tableLayout, "Total Amount: ₱ " + model.TotalAmount, 3);
            AddCellToBody(tableLayout, "Delivery Type: " + model.DeliveryType, 3);
            AddCellToBody(tableLayout, "Date Ordered: " + model.DateOrdered, 3);
            AddCellToBody(tableLayout, "Date Printed: " + model.DatePrinted, 3);

            return tableLayout;
        }

        //--> Method to add single cell to the Header  
        private static void AddCellToHeader(PdfPTable tableLayout, string cellText)
        {

            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL, BaseColor.WHITE)))
            {
                HorizontalAlignment = Element.ALIGN_CENTER,
                VerticalAlignment = Element.ALIGN_MIDDLE,
                FixedHeight = 20,
                Padding = 1,
                BorderWidth = 1,
                BackgroundColor = new BaseColor(107, 142, 45)
            });
        }

        // Method to add single cell to the body  
        private static void AddCellToBody(PdfPTable tableLayout, string cellText, int colspan = 1)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                VerticalAlignment = Element.ALIGN_MIDDLE,
                FixedHeight = 15,
                Padding = 1,
                BorderWidth = 1,
                Colspan = colspan,
                BackgroundColor = new BaseColor(255, 255, 255)
            });
        }

        private static void AddCellToBodyNoBorder(PdfPTable tableLayout, string cellText, int colspan = 1)
        {
            tableLayout.AddCell(new PdfPCell(new Phrase(cellText, new Font(Font.FontFamily.HELVETICA, 9, Font.NORMAL, BaseColor.BLACK)))
            {
                HorizontalAlignment = Element.ALIGN_LEFT,
                VerticalAlignment = Element.ALIGN_MIDDLE,
                FixedHeight = 15,
                Padding = 1,
                BorderWidth = 0,
                Colspan = colspan,
                BackgroundColor = new BaseColor(255, 255, 255)
            });
        }
    }
}