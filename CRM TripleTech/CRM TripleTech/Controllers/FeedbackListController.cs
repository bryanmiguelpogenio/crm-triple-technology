﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class FeedbackListController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public FeedbackListController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Feedback
        public ActionResult Index(FeedbackViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.drp_Client = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            var feedbacktransaction = unitOfWork.FeedbackTran.SearchFilter(model);

            model.lst_Main = new List<FeedbackModel>();
            foreach (var item in feedbacktransaction)
            {
                item.CLIENT = unitOfWork.User.Get(item.CLIENT_ID);

                if (item.CLIENT != null)
                {
                    model.lst_Main.Add(new FeedbackModel
                    {
                        Id = item.ID.ToString(),
                        Subject = item.SUBJECT,
                        Status = item.PRIORITY,
                        CustomerName = item.CLIENT_ID != Constant.NO_USER_ID ? item.CLIENT.FIRSTNAME + " " + item.CLIENT.LASTNAME : string.Empty,
                        CompanyName = item.CLIENT.COMPANY_ID.HasValue & item.CLIENT.COMPANY_ID != 0 ? item.CLIENT.COMPANY.NAME : string.Empty,
                        HasReplied = !string.IsNullOrEmpty(item.ADMIN_RESPONSE) ? true : false
                    });
                }
            }

            return View(model);
        }

        public ActionResult Details(int id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var model = new AdminFeedbackViewModel();
            var feedbacktran = unitOfWork.FeedbackTran.Get(id);
            var user = unitOfWork.User.Get(feedbacktran.CLIENT_ID);

            model.Id = feedbacktran.ID;
            model.Subject = feedbacktran.SUBJECT;
            model.ClientMessage = feedbacktran.USER_COMPLAINT;
            model.CustomerName = user.FIRSTNAME + " " + user.LASTNAME;
            model.CompanyName = user.COMPANY != null & user.COMPANY_ID != 0 ? user.COMPANY.NAME : string.Empty;
            model.AdminMessage = feedbacktran.ADMIN_RESPONSE;

            return View(model);
        }
    }
}