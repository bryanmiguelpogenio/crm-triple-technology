﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.OrderTran;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Autofac;
using CRM_TripleTech.Models.Interface;

namespace CRM_TripleTech.Controllers
{
    public class OrderTranPriorityController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public OrderTranPriorityController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }
        // GET: OrderTranPriority
        public ActionResult Index(OrderTranViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var ordertransactions = unitOfWork.OrderTran.getActiveEntities(model);

            model.lst_Main = new List<OrderTranModel>();
            foreach (var item in ordertransactions)
            {
                model.lst_Main.Add(new OrderTranModel
                {
                    Id = item.ID,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Location = item.LOCATION,
                    Status = item.STATUS,
                    TotalPrice = item.TOTALPRICE
                });
            }

            model.drp_Status = new List<SelectListItem>();

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_ACTIVE,
                Value = Constant.ORDERTRAN_STATUS_ACTIVE
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_PROCESSING,
                Value = Constant.ORDERTRAN_STATUS_PROCESSING
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_SHIPPING,
                Value = Constant.ORDERTRAN_STATUS_SHIPPING
            });

            return View(model);
        }

        public ActionResult UpdateStatus(int id, string transaction)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var ordertran = unitOfWork.OrderTran.Get(id);

            switch (transaction)
            {
                case Constant.PRIORITY_LEVEL_HIGH:
                    ordertran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_HIGH;
                    break;
                case Constant.PRIORITY_LEVEL_MED:
                    ordertran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_MED;
                    break;
                case Constant.PRIORITY_LEVEL_LOW:
                    ordertran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_LOW;
                    break;
            }

            unitOfWork.OrderTran.Update(ordertran);
            unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}