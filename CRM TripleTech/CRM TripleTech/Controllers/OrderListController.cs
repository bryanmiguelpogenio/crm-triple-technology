﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.OrderTran;
using System.Collections.Generic;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class OrderListController : Controller
    {
        // GET: OrderList
        private readonly IUnitOfWork unitOfWork;

        public OrderListController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        public ActionResult Index(OrderTranViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var ordertransactions = unitOfWork.OrderTran.getEntities(model);

            model.lst_Main = new List<OrderTranModel>();
            foreach (var item in ordertransactions)
            {
                model.lst_Main.Add(new OrderTranModel
                {
                    Id = item.ID,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Location = item.LOCATION,
                    Status = item.STATUS,
                    TotalPrice = item.TOTALPRICE
                });
            }

            model.drp_Status = new List<SelectListItem>();

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_ACTIVE,
                Value = Constant.ORDERTRAN_STATUS_ACTIVE
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_PROCESSING,
                Value = Constant.ORDERTRAN_STATUS_PROCESSING
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_SHIPPING,
                Value = Constant.ORDERTRAN_STATUS_SHIPPING
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_COMPLETED,
                Value = Constant.ORDERTRAN_STATUS_COMPLETED
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_CANCELLED,
                Value = Constant.ORDERTRAN_STATUS_CANCELLED
            });

            return View(model);
        }
    }
}