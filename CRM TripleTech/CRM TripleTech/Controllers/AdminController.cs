﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels;
using CRM_TripleTech.Models.ViewModels.Admin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.Serialization;
using System.Web.Mvc;
using System.Web.Script.Serialization;

namespace CRM_TripleTech.Controllers
{
    public class AdminController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        // GET: Account
        public AdminController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Admin
        public ActionResult Index()
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            //--> Number of products per category
            var lst_DataPoint = unitOfWork.Inventory.GetAll().GroupBy(x => x.CATEGORY_ID).Select(x => new DataPoint
            {
                Label = x.FirstOrDefault().CATEGORY.NAME,
                Y = x.Count()
            }).ToList();

            ConvertDataPointToChart(lst_DataPoint, "ProductsPerCategory");

            //--> Top 5 most bought products
            lst_DataPoint = unitOfWork.OrderTranDetail.GetAll().GroupBy(x => x.INVENTORY_ID).OrderByDescending(x => x.Sum(y => y.QUANTITY)).Take(5).Select(x => new DataPoint
            {
                Label = x.FirstOrDefault().INVENTORY.NAME,
                Y = x.Sum(y => y.QUANTITY)
            }).ToList();

            ConvertDataPointToChart(lst_DataPoint, "MostBoughtProducts");

            //--> Number of stocks of each product
            lst_DataPoint = unitOfWork.Inventory.GetAll().OrderBy(x => x.QUANTITY).Select(x => new DataPoint
            {
                Label = x.ID.ToString(),
                Y = x.QUANTITY
            }).ToList();

            ConvertDataPointToChart(lst_DataPoint, "NumberOfStocksPerProduct");

            //--> Top users who buys our products
            lst_DataPoint = unitOfWork.OrderTranDetail.GetAll().GroupBy(x => x.ORDERTRAN.USER_ID).OrderBy(x => x.Sum(y => y.QUANTITY)).Select(x => new DataPoint
            {
                Label = x.FirstOrDefault().ORDERTRAN.USER.USERNAME,
                Y = x.Sum(y => y.QUANTITY)
            }).ToList();

            ConvertDataPointToChart(lst_DataPoint, "TopUsers");

            //--> Number of orders/transactions per month
            lst_DataPoint = unitOfWork.OrderTran.GetAll().Where(x => x.DATEUPDATED.Value.Year == DateTime.UtcNow.Year).GroupBy(x => x.DATEUPDATED.Value.Month).OrderBy(x => x.FirstOrDefault().DATEUPDATED.Value.Month).Select(x => new DataPoint
            {
                Label = x.FirstOrDefault().DATEUPDATED.Value.Month.ToString(),
                Y = x.Count()
            }).ToList();

            lst_DataPoint.ForEach(x => { x.Label = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(int.Parse(x.Label)); });

            ConvertDataPointToChart(lst_DataPoint, "OrdersPerMonth");
            
            //--> Total sales each month
            lst_DataPoint = unitOfWork.OrderTran.Find(x => x.DATEUPDATED.Value.Year == DateTime.UtcNow.Year).GroupBy(x => x.DATEUPDATED.Value.Month).OrderBy(x => x.FirstOrDefault().DATEUPDATED.Value.Month).Select(x => new DataPoint
            {
                Label = x.FirstOrDefault().DATEUPDATED.Value.Month.ToString(),
                Y = x.Sum(y => y.TOTALPRICE)
            }).ToList();

            lst_DataPoint.ForEach(x => { x.Label = DateTimeFormatInfo.CurrentInfo.GetAbbreviatedMonthName(int.Parse(x.Label)); });

            ConvertDataPointToChart(lst_DataPoint, "TotalSalesPerMonth");

            AdminViewModel model = new AdminViewModel();

            var tempInventory = unitOfWork.Inventory.GetAll();

            model.lst_LCategory = new List<LegendInventory>();
            foreach (var item in tempInventory)
            {
                model.lst_LCategory.Add(new LegendInventory
                {
                    ID = item.ID,
                    Name = item.NAME
                });
            }

            return View(model);
        }

        [DataContract]
        public class DataPoint
        {
            [DataMember(Name = "label")]
            public string Label = null;

            [DataMember(Name = "y")]
            public double? Y = null;
        }

        public void ConvertDataPointToChart(List<DataPoint> lst_dataPoint, string sessionName)
        {
            #region STORE MODEL IN DASHBOARD DETAILS
            ChartsDetails modelchart = new ChartsDetails();
            JavaScriptSerializer serializer = new JavaScriptSerializer();

            modelchart.labels = new List<string>();
            modelchart.data = new List<double?>();
            foreach (var item in lst_dataPoint)
            {
                modelchart.labels.Add(item.Label);
                modelchart.data.Add(item.Y);
            }

            modelchart.maxValue = modelchart.data.Max();

            Session[sessionName] = serializer.Serialize(modelchart);
            #endregion
        }

        public JsonResult GetJSONData(string session)
        {
            var serializer = new JavaScriptSerializer();
            string serial = Session[session].ToString();
            //Session.Remove(session);

            return Json(serial);
        }

        public PartialViewResult GetAdminNavbarNotification()
        {
            var products = unitOfWork.Inventory.GetAll()
                .Where(x => x.QUANTITY <= 50)
                .Select(x =>
                {
                    string header = x.QUANTITY > 10 ? "Low Quantity" : "Critical";
                    string @class = x.QUANTITY > 10 ? "card-header-warning" : "card-header-danger";
                    return new AppNotification
                    {
                        Header = $"{header}: {x.NAME}",
                        Body = $"Remaining Quantity: {x.QUANTITY}",
                        Class = @class
                    };
                })
                .ToList();

            var services = unitOfWork.ServiceTran.GetAll()
                .SelectMany(x => x.USERSERVICESCHEDS)
                .Where(x => x.DATESCHEDULED.Value.Date == DateTime.UtcNow.Date)
                .Select(x => new AppNotification
                {
                    Header = $"Service Scheduled Today",
                    Body = $"Service: {x.SERVICETRAN.SERVICE.NAME}\n" +
                    $"Location: {x.SERVICETRAN.LOCATION}\n" +
                    $"Customer: {x.SERVICETRAN.USER.USERNAME}",
                    Class = "card-header-info"
                });

            return PartialView("~/Views/Shared/_AdminNavbarNotification.cshtml", products.Concat(services).ToList());
        }
    }
}