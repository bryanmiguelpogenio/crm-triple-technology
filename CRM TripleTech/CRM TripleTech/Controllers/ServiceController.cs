﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Category;
using CRM_TripleTech.Models.ViewModels.Service;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class ServiceController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ServiceController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Service
        public ActionResult Index(ServiceViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");
            
                model.lst_Main = unitOfWork.Service.SearchFilter(model);

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Add(CategoryAddViewModel model)
        {
            if (ModelState.IsValid)
            {
                    unitOfWork.Service.Add(new SERVICE
                    {
                        NAME = model.Name,
                        DESCRIPTION = model.Description,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow
                    });

                    unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            return PartialView("_Add", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(CategoryEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                    unitOfWork.Service.Update(new SERVICE
                    {
                        ID = model.Id,
                        NAME = model.Name,
                        DESCRIPTION = model.Description,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow
                    });

                    unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            return PartialView("_Edit", model);
        }

        public ActionResult Delete(string id)
        {
                int value = 0;
                int.TryParse(id, out value);

                var model = unitOfWork.Service.Get(value);
                unitOfWork.Service.Remove(model);

                unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_ADD:
                    ServiceAddViewModel addViewModel = new ServiceAddViewModel();
                    paramContent = RenderPartialViewToString("_ModalServiceAdd", addViewModel);
                    break;
                case Constant.MODAL_EDIT:
                    ServiceEditViewModel editViewModel = new ServiceEditViewModel();
                    
                        int value = 0;
                        int.TryParse(id, out value);
                        var service = unitOfWork.Service.Get(value);

                        editViewModel.Id = service.ID;
                        editViewModel.Name = service.NAME;
                        editViewModel.Description = service.DESCRIPTION;

                    paramContent = RenderPartialViewToString("_ModalServiceEdit", editViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}