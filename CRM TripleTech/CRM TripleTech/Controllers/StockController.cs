﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Inventory;
using CRM_TripleTech.Models.ViewModels.Stock;
using System;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class StockController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public StockController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Replenish
        public ActionResult Index(InventoryViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.Inventory.SearchFilter(model);

            model.drp_Category = unitOfWork.Category.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Replenish(StockReplenishViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                //var inventory = unitOfWork.Inventory.Get(model.InventoryID);

                //inventory.QUANTITY = inventory.QUANTITY + model.Quantity;
                //inventory.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                //inventory.DATEUPDATED = DateTime.UtcNow;

                //unitOfWork.Inventory.Update(inventory);

                var purchaseOrder = new PURCHASEORDER();
                int value = 0;

                purchaseOrder.PURCHASENO = Constant.DEFAULT_CODE;
                purchaseOrder.INVENTORY_ID = model.InventoryID;
                purchaseOrder.DATEPURCHASED = DateTime.UtcNow;
                purchaseOrder.BUYER_ID = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

                int.TryParse(model.Company_ID, out value);
                purchaseOrder.COMPANY_ID = value;

                purchaseOrder.QUANTITY = model.Quantity;
                purchaseOrder.AMOUNT = 0;
                purchaseOrder.STATUS = Constant.PURCHASE_ORDER_STATUS_PROCESSING;
                purchaseOrder.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                purchaseOrder.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.PurchaseOrder.Add(purchaseOrder);

                unitOfWork.SaveChanges();

                purchaseOrder.PURCHASENO = StaticMethods.CreatePurchaseNo(purchaseOrder.ID.ToString());
                unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            model.lst_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_VENDOR)
                .Select(m => new SelectListItem
                {
                    Text = m.NAME,
                    Value = m.ID.ToString()
                }).ToList();

            return PartialView("_Replenish", model);
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_REPLENISH:
                    StockReplenishViewModel replenishViewModel = new StockReplenishViewModel();

                    int value = 0;
                    int.TryParse(id, out value);
                    replenishViewModel.InventoryID = value;

                    replenishViewModel.lst_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_VENDOR)
                        .Select(m => new SelectListItem
                        {
                            Text = m.NAME,
                            Value = m.ID.ToString()
                        }).ToList();

                    paramContent = RenderPartialViewToString("_ModalStock", replenishViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}