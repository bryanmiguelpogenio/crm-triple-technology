﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Category;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class CategoryController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public CategoryController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Category
        public ActionResult Index(CategoryViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.Category.SearchFilter(model);

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Add(CategoryAddViewModel model)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Category.Add(new CATEGORY
                {
                    NAME = model.Name,
                    DESCRIPTION = model.Description,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            return PartialView("_Add", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(CategoryEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Category.Update(new CATEGORY
                {
                    ID = model.Id,
                    NAME = model.Name,
                    DESCRIPTION = model.Description,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            return PartialView("_Edit", model);
        }

        public ActionResult Delete(string id)
        {
            int value = 0;
            int.TryParse(id, out value);

            var model = unitOfWork.Category.Get(value);
            unitOfWork.Category.Remove(model);

            unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_ADD:
                    CategoryAddViewModel addViewModel = new CategoryAddViewModel();
                    paramContent = RenderPartialViewToString("_ModalCategoryAdd", addViewModel);
                    break;
                case Constant.MODAL_EDIT:
                    CategoryEditViewModel editViewModel = new CategoryEditViewModel();

                    int value = 0;
                    int.TryParse(id, out value);
                    var category = unitOfWork.Category.Get(value);

                    editViewModel.Id = category.ID;
                    editViewModel.Name = category.NAME;
                    editViewModel.Description = category.DESCRIPTION;

                    paramContent = RenderPartialViewToString("_ModalCategoryEdit", editViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}