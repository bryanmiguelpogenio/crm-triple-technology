﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class UserController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public UserController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: User
        public ActionResult Index(UserViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.User.SearchFilter(model);

            model.drp_Role = unitOfWork.Role.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_User = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Company = unitOfWork.Company.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult Details(string actiontype, int? id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");


            UserDetailViewModel model = new UserDetailViewModel();
            model.ActionType = actiontype;

            model.drp_Role = unitOfWork.Role.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Company = unitOfWork.Company.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            if (id.HasValue && id.Value != Constant.NO_USER_ID)
            {
                var user = unitOfWork.User.Get(id.Value);

                model.USER_ID = user.ID;
                model.ROLE_ID = user.ROLE_ID;
                model.COMPANY_ID = model.COMPANY_ID.HasValue ? model.COMPANY_ID.Value : 0;
                model.FIRSTNAME = user.FIRSTNAME;
                model.MIDDLENAME = user.MIDDLENAME;
                model.LASTNAME = user.LASTNAME;
                model.CONTACTNO = user.CONTACTNO;
                model.EMAILADDRESS = user.EMAILADDRESS;
                model.TELEPHONENO = user.TELEPHONENO;
                model.USERNAME = user.USERNAME;

                var address = unitOfWork.Address.getLatestEntityByUserID(id.Value);

                model.ADDRESS_ID = Constant.NO_ADDRESS_ID;
                if (address != null)
                {
                    model.ADDRESS_ID = address.ID;
                    model.ADDRESSNAME = address.ADDRESSNAME;
                    model.BARANGAY = address.BARANGAY;
                    model.MUNICIPALITY = address.MUNICIPALITY;
                    model.POSTALCODE = address.POSTALCODE;
                    model.LOCALITY = address.LOCALITY;
                }
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Add(UserDetailViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                var user = new USER();

                //user.ID = model.USER_ID;
                user.FIRSTNAME = model.FIRSTNAME;
                user.MIDDLENAME = model.MIDDLENAME;
                user.LASTNAME = model.LASTNAME;
                user.COMPANY_ID = model.COMPANY_ID;
                user.ROLE_ID = model.ROLE_ID;
                user.CONTACTNO = model.CONTACTNO;
                user.EMAILADDRESS = model.EMAILADDRESS;
                user.TELEPHONENO = model.TELEPHONENO;
                user.USERNAME = model.USERNAME;
                user.PASSWORD = Constant.DEFAULT_PASSWORD;

                user.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                user.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.User.Add(user);

                unitOfWork.Address.Add(new ADDRESS
                {
                    USER_ID = model.USER_ID,
                    ADDRESSNAME = model.ADDRESSNAME,
                    BARANGAY = model.BARANGAY,
                    MUNICIPALITY = model.MUNICIPALITY,
                    POSTALCODE = model.POSTALCODE,
                    LOCALITY = model.LOCALITY,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow,
                });

                unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            model.drp_Role = unitOfWork.Role.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Company = unitOfWork.Company.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View("Details", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(UserDetailViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {

                var user = unitOfWork.User.Get(model.USER_ID);

                user.ID = model.USER_ID;
                user.FIRSTNAME = model.FIRSTNAME;
                user.MIDDLENAME = model.MIDDLENAME;
                user.LASTNAME = model.LASTNAME;
                user.COMPANY_ID = model.COMPANY_ID.HasValue ? model.COMPANY_ID.Value : 0;
                user.ROLE_ID = model.ROLE_ID;
                user.CONTACTNO = model.CONTACTNO;
                user.EMAILADDRESS = model.EMAILADDRESS;
                user.TELEPHONENO = model.TELEPHONENO;
                user.USERNAME = model.USERNAME;

                user.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                user.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.User.Update(user);

                if (model.ADDRESS_ID != 0)
                {
                    var address = unitOfWork.Address.Get(model.ADDRESS_ID);

                    address.ID = model.ADDRESS_ID;
                    address.USER_ID = model.USER_ID;
                    address.ADDRESSNAME = model.ADDRESSNAME;
                    address.BARANGAY = model.BARANGAY;
                    address.MUNICIPALITY = model.MUNICIPALITY;
                    address.POSTALCODE = model.POSTALCODE;
                    address.LOCALITY = model.LOCALITY;

                    address.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                    address.DATEUPDATED = DateTime.UtcNow;

                    unitOfWork.Address.Update(address);
                }
                else
                {
                    unitOfWork.Address.Add(new ADDRESS
                    {
                        USER_ID = model.USER_ID,
                        ADDRESSNAME = model.ADDRESSNAME,
                        BARANGAY = model.BARANGAY,
                        MUNICIPALITY = model.MUNICIPALITY,
                        POSTALCODE = model.POSTALCODE,
                        LOCALITY = model.LOCALITY,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow,
                    });
                }

                unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            model.drp_Role = unitOfWork.Role.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Company = unitOfWork.Company.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View("Details", model);
        }
    }
}