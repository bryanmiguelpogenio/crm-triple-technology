﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.OrderTran;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class OrderTranController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public OrderTranController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: OrderTran
        public ActionResult Index(OrderTranViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            //var ordertransactions = unitOfWork.OrderTran.getForTransactionEntities(model)
            //    .OrderByDescending(m => m.ID)
            //    .ThenBy(m => m.PRIORITYLEVEL == Constant.PRIORITY_LEVEL_LOW)
            //    .ThenBy(m => m.PRIORITYLEVEL == Constant.PRIORITY_LEVEL_MED)
            //    .ThenBy(m => m.PRIORITYLEVEL == Constant.PRIORITY_LEVEL_HIGH)
            //    .ToList();

            var ordertransactions = unitOfWork.OrderTran.getForTransactionEntities(model)
                .OrderBy(m => m.PRIORITYLEVEL == Constant.PRIORITY_LEVEL_LOW)
                .ThenBy(m => m.PRIORITYLEVEL == Constant.PRIORITY_LEVEL_MED)
                .ThenBy(m => m.PRIORITYLEVEL == Constant.PRIORITY_LEVEL_HIGH)
                .ThenByDescending(m => m.ID);

            model.lst_Main = new List<OrderTranModel>();
            foreach (var item in ordertransactions)
            {
                model.lst_Main.Add(new OrderTranModel
                {
                    Id = item.ID,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Location = item.LOCATION,
                    Status = item.STATUS,
                    TotalPrice = item.TOTALPRICE
                });
            }

            model.drp_Status = new List<SelectListItem>();

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_ACTIVE,
                Value = Constant.ORDERTRAN_STATUS_ACTIVE
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_PROCESSING,
                Value = Constant.ORDERTRAN_STATUS_PROCESSING
            });

            model.drp_Status.Add(new SelectListItem
            {
                Text = Constant.ORDERTRAN_STATUS_SHIPPING,
                Value = Constant.ORDERTRAN_STATUS_SHIPPING
            });

            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (!id.HasValue || id.Value == Constant.NO_ORDERTRAN_ID) return RedirectToAction("Index");

            OrderTranModel model = new OrderTranModel();

            var ordertran = unitOfWork.OrderTran.Get(id.Value);

            model.Id = ordertran.ID;
            model.CustomerName = ordertran.USER.FIRSTNAME + " " + ordertran.USER.LASTNAME;
            model.Company = ordertran.USER.COMPANY_ID != 0 ? ordertran.USER.COMPANY.NAME : string.Empty;
            model.PriorityLevel = ordertran.PRIORITYLEVEL;
            model.Location = ordertran.LOCATION;
            model.Status = ordertran.STATUS;
            model.TotalPrice = ordertran.TOTALPRICE;

            model.DateDelivered = ordertran.DATEDELIVERED;
            model.DateUpdated = ordertran.DATEUPDATED;

            var user = unitOfWork.User.Get(ordertran.LASTUPDATEDBY);
            model.LastUpdatedBy = user != null ? user.FIRSTNAME + " " + user.LASTNAME : string.Empty;

            model.lst_Main = new List<OrderTranDetailModel>();

            var lst_ordertrandetail = ordertran.ORDERTRANDETAIL.ToList();
            foreach (var item in lst_ordertrandetail)
            {
                var price = item.PRICE / item.QUANTITY;

                model.lst_Main.Add(new OrderTranDetailModel
                {
                    Id = item.ID,
                    CategoryName = item.INVENTORY.CATEGORY.NAME,
                    Name = item.INVENTORY.NAME,
                    Price = price,
                    Quantity = item.QUANTITY,
                    Amount = item.PRICE,
                    Status = item.STATUS
                });
            }

            return View(model);
        }

        public ActionResult UpdateStatus(int id, string transaction)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var ordertran = unitOfWork.OrderTran.Get(id);
            var ordertrandetails = new List<ORDERTRANDETAIL>();
            string value = string.Empty;

            switch (transaction)
            {
                case Constant.ORDERTRAN_TRANSACTION_TYPE_SHIP:
                    ordertran.STATUS = Constant.ORDERTRAN_STATUS_SHIPPING;
                    ordertrandetails = ordertran.ORDERTRANDETAIL.Where(m => m.STATUS != Constant.ORDERTRAN_STATUS_CANCELLED).ToList();
                    value = Constant.ORDERTRAN_STATUS_SHIPPING;
                    break;
                case Constant.ORDERTRAN_TRANSACTION_TYPE_COMPLETE:
                    ordertran.DATEDELIVERED = DateTime.UtcNow;
                    ordertran.STATUS = Constant.ORDERTRAN_STATUS_COMPLETED;
                    ordertrandetails = ordertran.ORDERTRANDETAIL.Where(m => m.STATUS != Constant.ORDERTRAN_STATUS_CANCELLED).ToList();
                    value = Constant.ORDERTRAN_STATUS_COMPLETED;
                    break;
                case Constant.ORDERTRAN_TRANSACTION_TYPE_CANCEL:
                    ordertran.STATUS = Constant.ORDERTRAN_STATUS_CANCELLED;
                    ordertrandetails = ordertran.ORDERTRANDETAIL.Where(m => m.STATUS != Constant.ORDERTRAN_STATUS_CANCELLED).ToList();
                    value = Constant.ORDERTRAN_STATUS_CANCELLED;
                    break;
            }

            ordertran.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
            ordertran.DATEUPDATED = DateTime.UtcNow;

            foreach (var item in ordertrandetails)
            {
                item.STATUS = value;
                item.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                item.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.OrderTranDetail.Update(item);
            }

            unitOfWork.OrderTran.Update(ordertran);

            unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}