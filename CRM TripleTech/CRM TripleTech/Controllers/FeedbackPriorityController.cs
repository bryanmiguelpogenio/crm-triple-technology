﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.ViewModels.Feedback;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class FeedbackPriorityController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public FeedbackPriorityController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: FeedbackPriority
        public ActionResult Index(FeedbackViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.drp_Client = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            var feedbacktransaction = unitOfWork.FeedbackTran.SearchFilter(model, m => m.ISDONE == false).ToList();

            model.lst_Main = new List<FeedbackModel>();
            foreach (var item in feedbacktransaction)
            {
                item.CLIENT = unitOfWork.User.Get(item.CLIENT_ID);

                if (item.CLIENT != null)
                {
                    model.lst_Main.Add(new FeedbackModel
                    {
                        Id = item.ID.ToString(),
                        Subject = item.SUBJECT,
                        Status = item.PRIORITY,
                        CustomerName = item.CLIENT_ID != Constant.NO_USER_ID ? item.CLIENT.FIRSTNAME + " " + item.CLIENT.LASTNAME : string.Empty,
                        CompanyName = item.CLIENT.COMPANY_ID != null & item.CLIENT.COMPANY_ID != 0 ? item.CLIENT.COMPANY.NAME : string.Empty,
                        HasReplied = !string.IsNullOrEmpty(item.ADMIN_RESPONSE) ? true : false
                    });
                }
            }

            return View(model);
        }

        public ActionResult UpdateStatus(int id, string transaction)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");
            
                var feedbacktran = unitOfWork.FeedbackTran.Get(id);

                switch (transaction)
                {
                    case Constant.PRIORITY_LEVEL_HIGH:
                        feedbacktran.PRIORITY = Constant.PRIORITY_LEVEL_HIGH;
                        break;
                    case Constant.PRIORITY_LEVEL_MED:
                        feedbacktran.PRIORITY = Constant.PRIORITY_LEVEL_MED;
                        break;
                    case Constant.PRIORITY_LEVEL_LOW:
                        feedbacktran.PRIORITY = Constant.PRIORITY_LEVEL_LOW;
                        break;
                }

                unitOfWork.FeedbackTran.Update(feedbacktran);
                unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}