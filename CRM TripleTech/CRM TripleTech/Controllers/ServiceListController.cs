﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Service;
using CRM_TripleTech.Models.ViewModels.ServiceTran;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class ServiceListController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ServiceListController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Service
        public ActionResult Index(ServiceTranViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = new List<ServiceTranModel>();
            var servicetran = unitOfWork.ServiceTran.SearchFilter(model);

            foreach (var item in servicetran)
            {
                var userservicetran = unitOfWork.UserServiceTran.getEntityByServiceTranID(item.ID);

                model.lst_Main.Add(new ServiceTranModel
                {
                    Id = item.ID,
                    ServiceName = item.SERVICE.NAME,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    Location = item.LOCATION,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Status = item.STATUS,
                    HasAssignedEmployee = userservicetran != null && userservicetran.USER_ID != Constant.NO_USER_ID ? true : false
                });
            }

            model.drp_User = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Service = unitOfWork.Service.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult Details(int? id)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            if (!id.HasValue || id.Value == Constant.NO_ORDERTRAN_ID) return RedirectToAction("Index");

            ServiceTranDetailViewModel model = new ServiceTranDetailViewModel();

            model.drp_User = unitOfWork.User.GetAll().Where(m => m.ROLE.NAME == Constant.ROLE_CREW).Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            var servicetran = unitOfWork.ServiceTran.Get(id.Value);
            var userservicetran = unitOfWork.UserServiceTran.getEntityByServiceTranID(id.Value);

            model.Main = new ServiceTranModel();

            model.Main.Id = servicetran.ID;
            model.Main.ServiceName = servicetran.SERVICE.NAME;
            model.Main.CustomerName = servicetran.USER.FIRSTNAME + " " + servicetran.USER.LASTNAME;
            model.Main.Company = servicetran.USER.COMPANY_ID != 0 ? servicetran.USER.COMPANY.NAME : string.Empty;
            model.Main.Location = servicetran.LOCATION;
            model.Main.PriorityLevel = servicetran.PRIORITYLEVEL;
            model.Main.Status = servicetran.STATUS;
            model.Main.HasAssignedEmployee = userservicetran != null && userservicetran.USER_ID != Constant.NO_USER_ID ? true : false;


            model.Main.ServiceSchedID = userservicetran.ID;
            model.Main.AssignedUser = userservicetran.USER_ID;
            model.Main.DateScheduled = userservicetran.DATESCHEDULED;
            model.Main.ServiceSchedStatus = userservicetran.STATUS;

            return View(model);
        }
    }
}