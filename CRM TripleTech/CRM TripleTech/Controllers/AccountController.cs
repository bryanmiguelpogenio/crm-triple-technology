﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Account;
using System;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class AccountController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        // GET: Account
        public AccountController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        public ActionResult Index()
        {
            AccountViewModel model = new AccountViewModel();
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(AccountViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = unitOfWork.User.Find(m => m.USERNAME == model.Username & m.PASSWORD == model.Password).FirstOrDefault();
                if (user != null)
                {
                    Session["UserID"] = user.ID.ToString();
                    Session["RoleID"] = user.ROLE_ID;

                    return RedirectToAction("Index", "Home");
                }
                //else
                //{
                //    return JavaScript("alert('Username or Password is incorrect.')");
                //}
            }

            return View();
        }

        public ActionResult Profile()
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index");

            ProfileViewModel model = new ProfileViewModel();
            model.drp_Role = unitOfWork.Role.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_NON_VENDOR).Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            var user = unitOfWork.User.Get(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);

            model.USER_ID = user.ID;
            model.ROLE_ID = user.ROLE_ID;
            model.COMPANY_ID = user.COMPANY_ID;
            model.FIRSTNAME = user.FIRSTNAME;
            model.MIDDLENAME = user.MIDDLENAME;
            model.LASTNAME = user.LASTNAME;
            model.CONTACTNO = user.CONTACTNO;
            model.EMAILADDRESS = user.EMAILADDRESS;
            model.TELEPHONENO = user.TELEPHONENO;
            model.USERNAME = user.USERNAME;

            var address = unitOfWork.Address.getLatestEntityByUserID(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);

            model.ADDRESS_ID = Constant.NO_ADDRESS_ID;
            if (address != null)
            {
                model.ADDRESS_ID = address.ID;
                model.ADDRESSNAME = address.ADDRESSNAME;
                model.BARANGAY = address.BARANGAY;
                model.MUNICIPALITY = address.MUNICIPALITY;
                model.POSTALCODE = address.POSTALCODE;
                model.LOCALITY = address.LOCALITY;
            }

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Profile(ProfileViewModel model)
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index");

            if (ModelState.IsValid)
            {
                var user = unitOfWork.User.Get(model.USER_ID);

                user.ID = model.USER_ID;
                user.FIRSTNAME = model.FIRSTNAME;
                user.MIDDLENAME = model.MIDDLENAME;
                user.LASTNAME = model.LASTNAME;
                user.COMPANY_ID = model.COMPANY_ID;
                user.CONTACTNO = model.CONTACTNO;
                user.EMAILADDRESS = model.EMAILADDRESS;
                user.TELEPHONENO = model.TELEPHONENO;
                user.USERNAME = model.USERNAME;

                user.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                user.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.User.Update(user);

                if (model.ADDRESS_ID != 0)
                {
                    var address = unitOfWork.Address.Get(model.ADDRESS_ID);

                    address.ID = model.ADDRESS_ID;
                    address.USER_ID = model.USER_ID;
                    address.ADDRESSNAME = model.ADDRESSNAME;
                    address.BARANGAY = model.BARANGAY;
                    address.MUNICIPALITY = model.MUNICIPALITY;
                    address.POSTALCODE = model.POSTALCODE;
                    address.LOCALITY = model.LOCALITY;

                    address.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                    address.DATEUPDATED = DateTime.UtcNow;

                    unitOfWork.Address.Update(address);
                }
                else
                {
                    unitOfWork.Address.Add(new ADDRESS
                    {
                        USER_ID = model.USER_ID,
                        ADDRESSNAME = model.ADDRESSNAME,
                        BARANGAY = model.BARANGAY,
                        MUNICIPALITY = model.MUNICIPALITY,
                        POSTALCODE = model.POSTALCODE,
                        LOCALITY = model.LOCALITY,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow,
                    });
                }

                unitOfWork.SaveChanges();

                return RedirectToAction("Profile");
            }

            model.drp_Role = unitOfWork.Role.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_NON_VENDOR).Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult Logout()
        {
            Session.Remove("UserID");
            Session.Remove("RoleID");

            return RedirectToAction("Index");
        }

        public ActionResult Register()
        {
            RegisterViewModel model = new RegisterViewModel();

            model.lst_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_NON_VENDOR).Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                var role = unitOfWork.Role.Get(Constant.ROLE_CUSTOMER_ID);

                unitOfWork.User.Add(new USER
                {
                    FIRSTNAME = model.FirstName,
                    MIDDLENAME = model.MiddleName,
                    LASTNAME = model.LastName,
                    EMAILADDRESS = model.EmailAddress,
                    COMPANY_ID = model.CompanyID.HasValue ? model.CompanyID.Value : 0,
                    USERNAME = model.Username,
                    PASSWORD = model.Password,
                    ROLE_ID = role.ID,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();

                return RedirectToAction("Index");
            }

            model.lst_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_NON_VENDOR).Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult ForgotPassword()
        {
            return View();
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = unitOfWork.User.Find(m => m.EMAILADDRESS == model.EmailAddress).FirstOrDefault();

                if (user != null)
                {
                    string email = model.EmailAddress;
                    string password = user.PASSWORD;

                    MailMessage message = new MailMessage();

                    string body = "<p>Hello <b>{0}</b>! here are your account details for your password.</p> " +
                                  "<p>Email From: {1}</p> " +
                                  "<p>Your password is: <b>{2}</b></p>" +
                                  "<br />" +
                                  "<p>Email is auto generated. Please do not reply thank you.</p>";

                    message.To.Add(new MailAddress(model.EmailAddress));  // replace with valid value 
                    message.From = new MailAddress("tripletechnoreply@gmail.com");  // replace with valid value
                    message.Subject = "Password Recovery";
                    message.Body = string.Format(body, user.FIRSTNAME + " " + user.LASTNAME, "TripleTech System", user.PASSWORD);
                    message.IsBodyHtml = true;

                    using (var smtp = new SmtpClient())
                    {
                        smtp.Host = "smtp.gmail.com";
                        smtp.Port = 587;
                        smtp.UseDefaultCredentials = false;
                        smtp.Credentials = new NetworkCredential("tripletechnoreply@gmail.com", "Tripletech123");
                        smtp.EnableSsl = true;
                        smtp.DeliveryMethod = SmtpDeliveryMethod.Network;

                        await smtp.SendMailAsync(message);
                        TempData["Message"] = "Email has been sent successfully";
                        return RedirectToAction("Index", "Account");
                    }
                }
            }

            return View(model);
        }

        public ActionResult ChangePassword()
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index", "Account");

            ChangePasswordViewModel model = new ChangePasswordViewModel();

            model.Id = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ChangePassword(ChangePasswordViewModel model)
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index", "Account");

            if (ModelState.IsValid)
            {
                var user = unitOfWork.User.Get(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);

                user.PASSWORD = model.NewPassword;

                user.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                user.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.User.Update(user);
                unitOfWork.SaveChanges();

                return RedirectToAction("Profile", "Account");
            }

            return View(model);
        }

        public ActionResult Transactions()
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index");

            TransactionViewModel model = new TransactionViewModel();
            var id = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

            model.lst_order = unitOfWork.OrderTran.Find(m => m.USER_ID == id).OrderByDescending(m => m.ID).ToList();
            model.lst_service = unitOfWork.ServiceTran.Find(m => m.USER_ID == id).OrderByDescending(m => m.ID).ToList();
            model.lst_feedback = unitOfWork.FeedbackTran.Find(m => m.CLIENT_ID == id).OrderByDescending(m => m.ID).ToList();

            return View(model);
        }

        public ActionResult OrderDetails(int? id)
        {
            if (StaticMethods.TryParseObjToInt(Session["UserID"]).Value == Constant.NO_USER_ID)
                return RedirectToAction("Index", "Account");

            if (!id.HasValue || id.Value == Constant.NO_ORDERTRAN_ID)
                return RedirectToAction("Transactions");

            OrderDetailViewModel model = new OrderDetailViewModel();
            model.ordertran = unitOfWork.OrderTran.Get(id.Value);

            return View(model);
        }

        public PartialViewResult GetAccountNavbarNotification()
        {
            var id = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

            var services = unitOfWork.ServiceTran.GetAll()
                .Where(x => x.USER_ID == id)
                .SelectMany(x => x.USERSERVICESCHEDS)
                .Where(x => x.DATESCHEDULED.Value.Date == DateTime.UtcNow.Date)
                .Select(x => new AppNotification
                {
                    Header = $"Service Scheduled Today",
                    Body = $"Service: {x.SERVICETRAN.SERVICE.NAME}\n" +
                    $"Location: {x.SERVICETRAN.LOCATION}\n" +
                    $"Customer: {x.SERVICETRAN.USER.USERNAME}",
                    Class = "card-header-info"
                })
                .ToList();

            return PartialView("~/Views/Shared/_AccountNotification.cshtml", services);
        }

        #region VALIDATORS
        [HttpPost]
        public JsonResult IsPasswordCorrect(string Password, string Username)
        {
            bool retval = false;

            var model = unitOfWork.User.Find(m => m.USERNAME == Username & m.PASSWORD == Password).FirstOrDefault();
            if (model != null) retval = true;

            return Json(retval);
        }

        [HttpPost]
        public JsonResult IsChangePasswordCorrect(string OldPassword)
        {
            bool retval = false;

            int Id = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
            var model = unitOfWork.User.Find(m => m.ID == Id & m.PASSWORD == OldPassword).FirstOrDefault();
            if (model != null) retval = true;

            return Json(retval);
        }

        [HttpPost]
        public JsonResult IsUserNameExist(string Username)
        {
            var model = unitOfWork.User.Find(m => m.USERNAME == Username).FirstOrDefault();
            return Json(model == null);
        }

        [HttpPost]
        public JsonResult Profile_IsUserNameExist(string USERNAME, int USER_ID)
        {
            var model = unitOfWork.User.Find(m => m.USERNAME == USERNAME & m.ID != USER_ID).FirstOrDefault();
            return Json(model == null);
        }

        [HttpPost]
        public JsonResult IsEmailNotYetExist(string EmailAddress)
        {
            var model = unitOfWork.User.Find(m => m.EMAILADDRESS == EmailAddress).FirstOrDefault();
            return Json(model == null);
        }

        [HttpPost]
        public JsonResult Change_IsEmailNotYetExist(string EmailAddress)
        {
            var model = unitOfWork.User.Find(m => m.EMAILADDRESS == EmailAddress).FirstOrDefault();
            return Json(model != null);
        }

        [HttpPost]
        public JsonResult Profile_IsEmailExist(string EMAILADDRESS, int USER_ID)
        {
            var model = unitOfWork.User.Find(m => m.EMAILADDRESS == EMAILADDRESS & m.ID != USER_ID).FirstOrDefault();
            return Json(model == null);
        }
        #endregion
    }
}