﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.ViewModels.ServiceTran;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class ServiceTranPriorityController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public ServiceTranPriorityController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: ServiceTranPriority
        public ActionResult Index(ServiceTranViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = new List<ServiceTranModel>();
            var servicetran = unitOfWork.ServiceTran.SearchFilter(model, m => m.STATUS != Constant.SERVICE_STATUS_COMPLETED);

            foreach (var item in servicetran)
            {
                var userservicetran = unitOfWork.UserServiceTran.getEntityByServiceTranID(item.ID);

                model.lst_Main.Add(new ServiceTranModel
                {
                    Id = item.ID,
                    ServiceName = item.SERVICE.NAME,
                    CustomerName = item.USER.FIRSTNAME + " " + item.USER.LASTNAME,
                    Company = item.USER.COMPANY_ID != 0 ? item.USER.COMPANY.NAME : string.Empty,
                    Location = item.LOCATION,
                    PriorityLevel = item.PRIORITYLEVEL,
                    Status = item.STATUS,
                    HasAssignedEmployee = userservicetran != null && userservicetran.USER_ID != Constant.NO_USER_ID ? true : false
                });
            }

            model.drp_User = unitOfWork.User.GetAll().Select(m => new SelectListItem
            {
                Text = m.FIRSTNAME + " " + m.LASTNAME,
                Value = m.ID.ToString()
            }).ToList();

            model.drp_Service = unitOfWork.Service.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult UpdateStatus(int id, string transaction)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var servicetran = unitOfWork.ServiceTran.Get(id);

            switch (transaction)
            {
                case Constant.PRIORITY_LEVEL_HIGH:
                    servicetran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_HIGH;
                    break;
                case Constant.PRIORITY_LEVEL_MED:
                    servicetran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_MED;
                    break;
                case Constant.PRIORITY_LEVEL_LOW:
                    servicetran.PRIORITYLEVEL = Constant.PRIORITY_LEVEL_LOW;
                    break;
            }

            unitOfWork.ServiceTran.Update(servicetran);
            unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }
    }
}