﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Inventory;
using System;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class InventoryController : Controller
    {
        private readonly IUnitOfWork unitOfWork;
        private const string FILE_PATH = "~/images/products";

        public InventoryController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Inventory
        public ActionResult Index(InventoryViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");
            
                model.lst_Main = unitOfWork.Inventory.SearchFilter(model);

                model.drp_Category = unitOfWork.Category.GetAll().Select(m => new SelectListItem
                {
                    Text = m.NAME,
                    Value = m.ID.ToString()
                }).ToList();

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Add(InventoryAddViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                    int quantity = 0;
                    int.TryParse(model.Quantity, out quantity);

                    float price = 0;
                    float.TryParse(model.Price, out price);

                    var inventory = new INVENTORY
                    {
                        NAME = model.Name,
                        DESCRIPTION = model.Description,
                        QUANTITY = quantity,
                        CATEGORY_ID = model.CategoryID,
                        PRICE = price,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow
                    };

                    unitOfWork.Inventory.Add(inventory);

                    unitOfWork.SaveChanges();

                    SaveImage(file, inventory.ID);

                return JavaScript("location.reload(true)");
            }

            model.lst_Category = unitOfWork.Category.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return PartialView("_Add", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(InventoryEditViewModel model, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                    int quantity = 0;
                    int.TryParse(model.Quantity, out quantity);

                    float price = 0;
                    float.TryParse(model.Price, out price);

                    var inventory = new INVENTORY
                    {
                        ID = model.Id,
                        NAME = model.Name,
                        DESCRIPTION = model.Description,
                        QUANTITY = quantity,
                        CATEGORY_ID = model.CategoryID,
                        PRICE = price,
                        LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                        DATEUPDATED = DateTime.UtcNow
                    };

                    unitOfWork.Inventory.Update(inventory);

                    unitOfWork.SaveChanges();

                    SaveImage(file, inventory.ID);

                return JavaScript("location.reload(true)");
            }

            model.lst_Category = unitOfWork.Category.GetAll().Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return PartialView("_Edit", model);
        }

        public ActionResult Delete(string id)
        {
                int value = 0;
                int.TryParse(id, out value);

                var model = unitOfWork.Inventory.Get(value);
                unitOfWork.Inventory.Remove(model);

                unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_ADD:
                    InventoryAddViewModel addViewModel = new InventoryAddViewModel();
                    
                        addViewModel.lst_Category = unitOfWork.Category.GetAll()
                            .OrderBy(m => m.NAME)
                            .Select(m => new SelectListItem
                            {
                                Text = m.NAME,
                                Value = m.ID.ToString()
                            }).ToList();

                    paramContent = RenderPartialViewToString("_ModalInventoryAdd", addViewModel);
                    break;
                case Constant.MODAL_EDIT:
                    InventoryEditViewModel editViewModel = new InventoryEditViewModel();
                    
                        int value = 0;
                        int.TryParse(id, out value);
                        var inventory = unitOfWork.Inventory.Get(value);

                        editViewModel.Id = inventory.ID;
                        editViewModel.Name = inventory.NAME;
                        editViewModel.Description = inventory.DESCRIPTION;
                        editViewModel.Quantity = inventory.QUANTITY.ToString();
                        editViewModel.CategoryID = inventory.CATEGORY_ID;
                        editViewModel.Price = inventory.PRICE.ToString();

                        editViewModel.lst_Category = unitOfWork.Category.GetAll()
                            .OrderBy(m => m.NAME)
                            .Select(m => new SelectListItem
                            {
                                Text = m.NAME,
                                Value = m.ID.ToString()
                            }).ToList();

                    paramContent = RenderPartialViewToString("_ModalInventoryEdit", editViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private void SaveImage(HttpPostedFileBase file, int inventoryId)
        {
            var imageStoragePath = Server.MapPath(FILE_PATH);

            if (file != null)
            {
                var fileName = inventoryId + ".jpg";//+ Path.GetExtension(file.FileName);
                var fullPath = Path.Combine(imageStoragePath, fileName);

                IsDirectoryExisting(imageStoragePath);

                {
                    var currentFiles = Directory.EnumerateFiles(imageStoragePath);
                    var existing = currentFiles.FirstOrDefault(x => Path.GetFileNameWithoutExtension(x) == inventoryId.ToString());
                    if (existing != null)
                    {
                        System.IO.File.Delete(existing);
                    }
                }

                file.SaveAs(fullPath);
            }
        }

        private void IsDirectoryExisting(string imageStoragePath)
        {
            bool exists = Directory.Exists(imageStoragePath);
            if (!exists)
                Directory.CreateDirectory(imageStoragePath);
        }
        #endregion
    }
}