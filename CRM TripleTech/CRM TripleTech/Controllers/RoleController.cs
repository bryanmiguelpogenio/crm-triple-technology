﻿using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.ViewModels.Role;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using CRM_TripleTech.Models.Interface;
using Autofac;

namespace CRM_TripleTech.Controllers
{
    public class RoleController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public RoleController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Role
        public ActionResult Index(RoleViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.Role.SearchFilter(model);

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Add(RoleAddViewModel model)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Role.Add(new ROLE
                {
                    NAME = model.Name,
                    DESCRIPTION = model.Description,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            return PartialView("_Add", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(RoleEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Role.Update(new ROLE
                {
                    ID = model.Id,
                    NAME = model.Name,
                    DESCRIPTION = model.Description,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();

                return JavaScript("location.reload(true)");
            }

            return PartialView("_Edit", model);
        }

        public ActionResult Delete(string id)
        {
            int value = 0;
            int.TryParse(id, out value);

            var model = unitOfWork.Role.Get(value);
            unitOfWork.Role.Remove(model);

            unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_ADD:
                    RoleAddViewModel addViewModel = new RoleAddViewModel();
                    paramContent = RenderPartialViewToString("_ModalRoleAdd", addViewModel);
                    break;
                case Constant.MODAL_EDIT:
                    RoleEditViewModel editViewModel = new RoleEditViewModel();

                    int value = 0;
                    int.TryParse(id, out value);
                    var role = unitOfWork.Role.Get(value);

                    editViewModel.Id = role.ID;
                    editViewModel.Name = role.NAME;
                    editViewModel.Description = role.DESCRIPTION;

                    paramContent = RenderPartialViewToString("_ModalRoleEdit", editViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}