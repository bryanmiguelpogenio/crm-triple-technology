﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.PurchaseOrder;
using System;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class PurchaseOrderController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public PurchaseOrderController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: PurchaseOrder
        public ActionResult Index(PurchaseOrderViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.PurchaseOrder.getEntitites(model, m => m.STATUS == Constant.PURCHASE_ORDER_STATUS_PROCESSING).ToList();

            model.lst_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_VENDOR).Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }

        public ActionResult UpdateStatus(int id, string transaction)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            var purchaseorder = unitOfWork.PurchaseOrder.Get(id);
            string value = string.Empty;

            switch (transaction)
            {
                case Constant.PURCHASE_ORDER_STATUS_RECEIVED:
                    purchaseorder.STATUS = Constant.PURCHASE_ORDER_STATUS_RECEIVED;

                    unitOfWork.PurchaseOrder.Update(purchaseorder);

                    var inventory = unitOfWork.Inventory.Get(purchaseorder.INVENTORY_ID);

                    inventory.QUANTITY += purchaseorder.QUANTITY;
                    inventory.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                    inventory.DATEUPDATED = DateTime.UtcNow;

                    unitOfWork.Inventory.Update(inventory);

                    unitOfWork.SaveChanges();
                    break;
            }

            return RedirectToAction("Index");
        }
    }
}