﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Cart;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class CartController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public CartController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Cart
        public ActionResult Index()
        {
            if (!StaticMethods.HasActiveOrderTransaction(StaticMethods.TryParseObjToInt(Session["UserID"])))
                return RedirectToAction("Index", "Account");

            List<CartViewModel> model = new List<CartViewModel>();
                var ordertran = unitOfWork.OrderTran.getActiveEntityByUserID(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);

                //--> Update price of each active items in the cart.
                //var ordertrandetails = unitOfWork.OrderTransactionDetailsRepository
                //    .getActiveEntitiesByUserID(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);

                var ordertrandetails = ordertran.ORDERTRANDETAIL.Where(m => m.STATUS == Constant.ORDERTRAN_STATUS_ACTIVE).ToList();

                foreach (var item in ordertrandetails)
                {
                    item.PRICE = item.QUANTITY * item.INVENTORY.PRICE;

                    model.Add(new CartViewModel
                    {
                        OrderTranId = ordertran.ID,
                        Id = item.ID,
                        Name = item.INVENTORY.NAME,
                        CategoryName = item.INVENTORY.CATEGORY.NAME,
                        Price = item.INVENTORY.PRICE,
                        Quantity = item.QUANTITY,
                        Amount = item.PRICE,
                        InventoryId = item.INVENTORY.ID
                    });

                    unitOfWork.OrderTranDetail.Update(item);
                }

                unitOfWork.SaveChanges();

            return View(model);
        }

        public ActionResult OrderItems(CartLocationViewModel model, int id = 0)
        {
            if (!StaticMethods.HasActiveOrderTransaction(StaticMethods.TryParseObjToInt(Session["UserID"])))
                return RedirectToAction("Index", "Account");

            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])) && string.IsNullOrEmpty(GetLocation(model)))
                return RedirectToAction("Index", "Cart");

            var userid = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;

            var ordertranid = id == 0 ? model.OrderTranId : id;
            var ordertran = unitOfWork.OrderTran.Get(ordertranid);

            //var ordertrandetails = unitOfWork.OrderTransactionDetailsRepository
            //    .getActiveEntitiesByUserID(userid);

            var ordertrandetails = ordertran.ORDERTRANDETAIL.Where(m => m.STATUS == Constant.ORDERTRAN_STATUS_ACTIVE).ToList();

            ordertran.LOCATION = !StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])) ? 
                GetLocation(model) : Constant.DEFAULT_DELIVERY_ADDRESS;
            ordertran.TOTALPRICE = ordertrandetails.Sum(m => m.PRICE);
            ordertran.STATUS = Constant.ORDERTRAN_STATUS_PROCESSING;
            ordertran.DATEORDERED = DateTime.UtcNow;
            ordertran.DATEDELIVERED = model.ScheduleDelivery;
            ordertran.LASTUPDATEDBY = userid;
            ordertran.DATEUPDATED = DateTime.UtcNow;

            unitOfWork.OrderTran.Update(ordertran);

            foreach (var item in ordertrandetails)
            {
                if (item.INVENTORY.QUANTITY >= item.QUANTITY)
                {
                    //--> deduct items from stocks.
                    item.INVENTORY.QUANTITY = item.INVENTORY.QUANTITY - item.QUANTITY;
                    item.STATUS = Constant.ORDERTRAN_STATUS_PROCESSING;

                    unitOfWork.Inventory.Update(item.INVENTORY);
                }
                else
                {
                    item.STATUS = Constant.ORDERTRAN_STATUS_CANCELLED;
                }

                item.LASTUPDATEDBY = userid;
                item.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.OrderTranDetail.Update(item);
            }

            unitOfWork.SaveChanges();

            return RedirectToAction("Index", "Home");
        }

        public ActionResult RemoveItemFromCart(int id)
        {
            var ordertrandetail = unitOfWork.OrderTranDetail.Get(id);

            ordertrandetail.STATUS = Constant.ORDERTRAN_STATUS_CANCELLED;
            ordertrandetail.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
            ordertrandetail.DATEUPDATED = DateTime.UtcNow;

            unitOfWork.OrderTranDetail.Update(ordertrandetail);

            var ordertran = unitOfWork.OrderTran.getActiveEntityByUserID(StaticMethods.TryParseObjToInt(Session["UserID"]).Value);
            var ordertrandetails = ordertran.ORDERTRANDETAIL.Where(m => m.STATUS == Constant.ORDERTRAN_STATUS_ACTIVE).ToList();

            if (ordertrandetails.Count == 0)
            {
                ordertran.STATUS = Constant.ORDERTRAN_STATUS_CANCELLED;
                ordertran.LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value;
                ordertran.DATEUPDATED = DateTime.UtcNow;

                unitOfWork.OrderTran.Update(ordertran);

                unitOfWork.SaveChanges();
                return RedirectToAction("Index", "Products");
            }

            unitOfWork.SaveChanges();

            return Json(new { message = Constant.ALERT_MESSAGE_CANCELLED }, JsonRequestBehavior.AllowGet);
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_CART:
                    CartLocationViewModel locationViewModel = new CartLocationViewModel();

                    locationViewModel.OrderTranId = StaticMethods.TryParseStringToInt(id).Value;

                    paramContent = RenderPartialViewToString("_ModalCartLocation", locationViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }

        private string GetLocation(CartLocationViewModel model)
        {
            string retval = string.Empty;

            retval = GetValue(model.UnitRmNoFloor) + GetValue(model.BldgName) + GetValue(model.LotNoBlockNoPhaseNoHouseNo)
                + GetValue(model.Street) + GetValue(model.Subdivision) + GetValue(model.Barangay) + GetValue(model.MunicipalityCity)
                + GetValue(model.ProvinceState) + GetValue(model.ZipCode);
            retval.Trim().TrimEnd(',');

            return retval;
        }

        private string GetValue(string value)
        {
            if (!string.IsNullOrEmpty(value)) return value + ", ";
            else return string.Empty;
        }
        #endregion
    }
}