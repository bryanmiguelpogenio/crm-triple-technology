﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.PurchaseOrder;
using System.Linq;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class PurchaseListController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public PurchaseListController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: PurchaseOrder
        public ActionResult Index(PurchaseOrderViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.PurchaseOrder.getEntitites(model).ToList();

            model.lst_Company = unitOfWork.Company.Find(m => m.TYPE == Constant.COMPANY_TYPE_VENDOR).Select(m => new SelectListItem
            {
                Text = m.NAME,
                Value = m.ID.ToString()
            }).ToList();

            return View(model);
        }
    }
}