﻿using Autofac;
using CRM_TripleTech.Models;
using CRM_TripleTech.Models.DAL;
using CRM_TripleTech.Models.Entities;
using CRM_TripleTech.Models.Interface;
using CRM_TripleTech.Models.ViewModels.Company;
using System;
using System.IO;
using System.Web.Mvc;

namespace CRM_TripleTech.Controllers
{
    public class CompanyController : Controller
    {
        private readonly IUnitOfWork unitOfWork;

        public CompanyController()
        {
            var container = ContainerConfig.Configure();
            var scope = container.BeginLifetimeScope();
            unitOfWork = scope.Resolve<IUnitOfWork>();
        }

        // GET: Company
        public ActionResult Index(CompanyViewModel model)
        {
            if (!StaticMethods.HasAdminAccess(StaticMethods.TryParseObjToInt(Session["RoleID"])))
                return RedirectToAction("Index", "Account");

            model.lst_Main = unitOfWork.Company.SearchFilter(model);

            return View(model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Add(CompanyAddViewModel model)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Company.Add(new COMPANY
                {
                    TYPE = model.Type,
                    CODE = model.Code,
                    NAME = model.Name,
                    ADDRESS = model.Address,
                    DESCRIPTION = model.Description,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();
                
                return JavaScript("location.reload(true)");
            }

            return PartialView("_Add", model);
        }

        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Edit(CompanyEditViewModel model)
        {
            if (ModelState.IsValid)
            {
                unitOfWork.Company.Update(new COMPANY
                {
                    ID = model.Id,
                    NAME = model.Name,
                    DESCRIPTION = model.Description,
                    LASTUPDATEDBY = StaticMethods.TryParseObjToInt(Session["UserID"]).Value,
                    DATEUPDATED = DateTime.UtcNow
                });

                unitOfWork.SaveChanges();
                
                return JavaScript("location.reload(true)");
            }

            return PartialView("_Edit", model);
        }

        public ActionResult Delete(string id)
        {
            int value = 0;
            int.TryParse(id, out value);

            var model = unitOfWork.Company.Get(value);
            unitOfWork.Company.Remove(model);

            unitOfWork.SaveChanges();

            return RedirectToAction("Index");
        }

        #region OTHERS
        public ActionResult GetParamContent(string content, string id)
        {
            string paramContent = string.Empty;
            switch (content)
            {
                case Constant.MODAL_ADD:
                    CompanyAddViewModel addViewModel = new CompanyAddViewModel();
                    paramContent = RenderPartialViewToString("_ModalCompanyAdd", addViewModel);
                    break;
                case Constant.MODAL_EDIT:
                    CompanyEditViewModel editViewModel = new CompanyEditViewModel();

                    int value = 0;
                    int.TryParse(id, out value);
                    var company = unitOfWork.Company.Get(value);

                    editViewModel.Id = company.ID;
                    editViewModel.Type = company.TYPE;
                    editViewModel.Code = company.CODE;
                    editViewModel.Name = company.NAME;
                    editViewModel.Description = company.DESCRIPTION;
                    editViewModel.Address = company.ADDRESS;

                    paramContent = RenderPartialViewToString("_ModalCompanyEdit", editViewModel);
                    break;
            }

            return Json(new { content = paramContent }, JsonRequestBehavior.AllowGet);

        }

        private string RenderPartialViewToString(string viewName, object model)
        {
            if (string.IsNullOrEmpty(viewName))
                viewName = ControllerContext.RouteData.GetRequiredString("action");

            ViewData.Model = model;

            using (var sw = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewName);
                var viewContext = new ViewContext(ControllerContext, viewResult.View, ViewData, TempData, sw);
                viewResult.View.Render(viewContext, sw);

                return sw.GetStringBuilder().ToString();
            }
        }
        #endregion
    }
}