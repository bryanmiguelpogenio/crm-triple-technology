﻿$(window).resize(function () {
    seq = seq2 = 0;

    setTimeout(function () {
        mdchart.initDashboardPageCharts();
    }, 500);
});

mdchart = {
    initDashboardPageCharts: function () {
        /* ----------==========     Daily Sales Chart initialization    ==========---------- */

		var sessions = ["ProductsPerCategory", "MostBoughtProducts", "NumberOfStocksPerProduct", "TopUsers", "OrdersPerMonth", "TotalSalesPerMonth"];

        for (var num = 0; num < sessions.length; num++) {
            var sessionName = sessions[num];
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({ 'session': sessionName }),
                async: false,
                url: "/Admin/GetJSONData",
                success: function (data) {
                    var arr = JSON.parse(data)

                    dataChart = {
                        labels: arr.labels,
                        series: [
                            arr.data
                        ]
                    };

                    var name = '#' + sessionName;
                    if (num % 2 == 0) {

                        optionsChart = {
                            lineSmooth: Chartist.Interpolation.cardinal({
                                tension: 0
                            }),
                            low: 0,
                            high: arr.maxValue + 1, // creative tim: we recommend you to set the high sa the biggest value + something for a better look
                            chartPadding: {
                                top: 0,
                                right: 0,
                                bottom: 0,
                                left: 0
                            },
                        }

                        var Chart = new Chartist.Line(name, dataChart, optionsChart);
                        mdchart.startAnimationForLineChart(Chart);
                    }
                    else {
                        optionsChart = {
                            axisX: {
                                showGrid: false
                            },
                            low: 0,
                            high: arr.maxValue + 1,
                            chartPadding: {
                                top: 0,
                                right: 5,
                                bottom: 0,
                                left: 0
                            }
                        }

                        var Chart = new Chartist.Bar(name, dataChart, optionsChart);
                        mdchart.startAnimationForBarChart(Chart);
                    }

                },
                error: function (result) { }
            });
        }
    },
    startAnimationForLineChart: function (chart) {
        chart.on('draw', function (data) {
            if ((data.type === 'line' || data.type === 'area') && window.matchMedia("(min-width: 900px)").matches) {
                data.element.animate({
                    d: {
                        begin: 600,
                        dur: 700,
                        from: data.path.clone().scale(1, 0).translate(0, data.chartRect.height()).stringify(),
                        to: data.path.clone().stringify(),
                        easing: Chartist.Svg.Easing.easeOutQuint
                    }
                });
            } else if (data.type === 'point') {
                seq++;
                data.element.animate({
                    opacity: {
                        begin: seq * delays,
                        dur: durations,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }

        });

        seq = 0;

    },
    startAnimationForBarChart: function (chart) {
        chart.on('draw', function (data) {
            if (data.type === 'bar' && window.matchMedia("(min-width: 900px)").matches) {
                seq2++;
                data.element.animate({
                    opacity: {
                        begin: seq2 * delays2,
                        dur: durations2,
                        from: 0,
                        to: 1,
                        easing: 'ease'
                    }
                });
            }

        });

        seq2 = 0;

    }
}